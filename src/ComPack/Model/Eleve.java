package ComPack.Model;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

import ComPack.Controllers.WorkSpaceController;
import application.MainApp;

public class Eleve extends Element{

	/**ajouter un compte Eleve**/
	public  void ajouter(String nom, String pw){
		try {
			pw=Model.toSHA1(pw.getBytes(StandardCharsets.UTF_8));
			this.nom=nom;
			String chemin="comptes/"+nom;
			new File(chemin).mkdirs();
			monFichier = new RandomAccessFile("f.txt", "rw");
			boolean trouv=Model.recherche(monFichier, nom);
			if(!trouv){
				monFichier.seek(monFichier.length());
				String enreg=nom+"#"+pw+"#elv"+"$";
				monFichier.writeUTF(enreg);
				WorkSpaceController.resett();
				MainApp.ShowWorkSpaceWindow();

			}
		}
		catch (Exception e) {e.printStackTrace();}
	}


}