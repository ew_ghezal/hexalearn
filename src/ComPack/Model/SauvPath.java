package ComPack.Model;

import java.io.Serializable;

public class SauvPath implements Serializable{
	/****/

	private static final long serialVersionUID = 1L;
	private String trajectoire,polygone;
	private double temps;
	private String type;
			/*getters and setters*/
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getTemps() {
		return temps;
	}

	public void setTemps(double temps) {
		this.temps = temps;
	}

	public String getTrajectoire() {
		return trajectoire;
	}

	public void setTrajectoire(String trajectoire) {
		this.trajectoire = trajectoire;
	}

	public String getPolygone() {
		return polygone;
	}

	public void setPolygone(String polygone) {
		this.polygone = polygone;
	}
}
