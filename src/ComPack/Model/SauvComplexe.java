package ComPack.Model;

import java.io.Serializable;

public class SauvComplexe implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private double fromX,fromY,fromScale,fromAngle,toX,toY,toScale,toAngle,fromDuree,toDuree,fromBleu,FromRouge,fromVert,toBleu,toRouge ,toVert;
	private String path,type;
	private int code;
	private double posx,posy;

	/* getters and setters*/
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getPosx() {
		return posx;
	}
	public void setPosx(double posx) {
		this.posx = posx;
	}
	public double getPosy() {
		return posy;
	}
	public void setPosy(double posy) {
		this.posy = posy;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public double getFromBleu() {
		return fromBleu;
	}
	public void setFromBleu(double fromBleu) {
		this.fromBleu = fromBleu;
	}
	public double getFromRouge() {
		return FromRouge;
	}
	public void setFromRouge(double fromRouge) {
		FromRouge = fromRouge;
	}
	public double getFromVert() {
		return fromVert;
	}
	public void setFromVert(double fromVert) {
		this.fromVert = fromVert;
	}
	public double getToBleu() {
		return toBleu;
	}
	public void setToBleu(double toBleu) {
		this.toBleu = toBleu;
	}
	public double getToRouge() {
		return toRouge;
	}
	public void setToRouge(double toRouge) {
		this.toRouge = toRouge;
	}
	public double getToVert() {
		return toVert;
	}
	public void setToVert(double toVert) {
		this.toVert = toVert;
	}
	public double getFromX() {
		return fromX;
	}
	public void setFromX(double fromX) {
		this.fromX = fromX;
	}
	public double getFromY() {
		return fromY;
	}
	public void setFromY(double fromY) {
		this.fromY = fromY;
	}
	public double getFromScale() {
		return fromScale;
	}
	public void setFromScale(double fromScale) {
		this.fromScale = fromScale;
	}
	public double getFromAngle() {
		return fromAngle;
	}
	public void setFromAngle(double fromAngle) {
		this.fromAngle = fromAngle;
	}
	public double getToX() {
		return toX;
	}
	public void setToX(double toX) {
		this.toX = toX;
	}
	public double getToY() {
		return toY;
	}
	public void setToY(double toY) {
		this.toY = toY;
	}
	public double getToScale() {
		return toScale;
	}
	public void setToScale(double toScale) {
		this.toScale = toScale;
	}
	public double getToAngle() {
		return toAngle;
	}
	public void setToAngle(double toAngle) {
		this.toAngle = toAngle;
	}
	public double getFromDuree() {
		return fromDuree;
	}
	public void setFromDuree(double fromDuree) {
		this.fromDuree = fromDuree;
	}
	public double getToDuree() {
		return toDuree;
	}
	public void setToDuree(double toDuree) {
		this.toDuree = toDuree;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
