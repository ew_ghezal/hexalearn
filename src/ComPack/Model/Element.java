package ComPack.Model;

import java.io.RandomAccessFile;

import java.nio.charset.StandardCharsets;

import ComPack.Controllers.WorkSpaceController;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import application.MainApp;
/** La classe element (Eleve ou enseignant ) avec ses diff�rents attributs  **/
public abstract class Element {

	 	protected static RandomAccessFile monFichier;
	 	protected  String nom=new String();
	 	protected String prenom=new String();
	 	protected String motDePasse=new String();
	 	protected String nomUtilisateur=new String();
	 	protected RandomAccessFile animFichier;

	 	/**Les diff�rents getters et setters **/
		public String getNom()
		{
			return nom;
		}

		public String getPrenom()
		{
			return prenom;
		}

		public String getNomUtilisateur()
		{
			return nomUtilisateur;
		}

		public String getMotDePasse()
		{
			return motDePasse;
		}

		public void setNom(String nom)
		{
			 this.nom=nom;
		}

		public void setPrenom(String prenom)
		{
			this.prenom=prenom;
		}

		public void setNomUtilisateur(String nomUtilisateur)
		{
			this.nomUtilisateur=nomUtilisateur;

		}

		public void setMotDePasse(String motDePasse)
		{
			this.motDePasse=motDePasse;

		}

		/** La fonction qui nous permet de se connecter **/
		public static void seConnecter(String nom,String pw){
			Alert alert = new Alert(AlertType.WARNING);
			try{
				monFichier = new RandomAccessFile("f.txt", "rw");
				pw=Model.toSHA1(pw.getBytes(StandardCharsets.UTF_8));
				String chaine=nom+"#"+pw+"#elv"+"$";
				boolean trouv=Model.recherche(monFichier, chaine);
				if(trouv){
					MainApp.element.setNom(nom);
					WorkSpaceController.resett();
					MainApp.ShowWorkSpaceWindow();


				}
				else {
					MainApp.playSound("warning");
					alert.setTitle("ATTENTION");
					alert.setHeaderText("Mot de passe ou nom d'utilisateur incorrect!" );
					alert.setContentText("Veuillez r�essayer.");
					alert.showAndWait();
				}
			}
			catch (Exception e){e.printStackTrace();}
			}
 /**ajouter un compte **/
		public abstract void ajouter(String nom, String pw);
	}