package ComPack.Model;

import static javafx.util.Duration.seconds;

import java.text.DecimalFormat;

import javafx.animation.PathTransition;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import application.MainApp;

public class Polygone extends Polygon{

	static double orgSceneX;
	static double orgSceneY;
    static double orgTranslateX;
	static double orgTranslateY;
    static double offsetX ;
    static double offsetY ;
    static double newTranslateX ;
    static double newTranslateY ;
	private double xCentre,yCentre,rayon,angle,scale;
	public static  PathTransition transition;
	private static Path path;
	static public double depx,depy,coefX,coefY,rotation;
	static int cp=0;
	private int nbCote;
	private Color couleur;
/* constructeur objet polygone*/

	public Polygone(){}

	public Polygone(double x,double y,double rayon,double angle,int nbCote, Color couleur){
		this.xCentre=x;
		this.yCentre=y;
		this.rayon=rayon;
		this.angle=angle;
		this.nbCote=nbCote;
		this.setCouleur(couleur);
		scale=1;
		depx=0;
		depy=0;
	}

	/* getters and setters*/
	public static Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		Polygone.path = path;
	}

	public static double getCoefX() {
		return coefX;
	}

	public static void setCoefX(double a) {
		coefX = a;
	}

	public static double getCoefY() {
		return coefY;
	}

	public static void setCoefY(double a) {
		coefY = a;
	}

	public static double getRotation() {
		return rotation;
	}

	public static void setRotation(double a) {
		rotation = a;
	}
	public static double getDepx() {
		return depx;
	}

	public static void setDepx(double x) {
		depx = x;
	}

	public static double getDepy() {
		return depy;
	}

	public static void setDepy(double y) {
		depy = y;
	}




	public double getxCentre() {
		return xCentre;
	}
	public void setxCentre(double xCentre) {
		this.xCentre = xCentre;
	}
	public double getyCentre() {
		return yCentre;
	}
	public void setyCentre(double yCentre) {
		this.yCentre = yCentre;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public void setRayon(double rayon){
		this.rayon=rayon;
	}
	public double getRayon(){
		return this.rayon;
	}
	public Color getCouleur() {
		return couleur;
	}
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	public Polygon getPolygone(){
		return this;
	}
	public int getNbCote() {
		return nbCote;
	}
	public void setNbCote(int nbCote) {
		this.nbCote = nbCote;
	}
	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public static String[] getTrajPoly(){
		String[] tab=new String[2];
		tab[0]=getPath().toString();
		tab[1]=Model.objetComplex.toString();
		return tab;
	}
	public static PathTransition getTransition(){
		return transition;
	}
/* affichage du polygone au centre de la grille*/
	public void afficherPolygone(){

		 double x[]=new double[nbCote];
		 double y[]=new double[nbCote];
		 double a=(360/nbCote);
		 double b=this.angle;

		 b=Math.toRadians(b);
		 a=Math.toRadians(a);
		 x[0]=(double)(rayon*Math.cos(b));
	     y[0]=(double)(rayon*Math.sin(b));
	     this.getPoints().add(x[0]+this.xCentre);
	     this.getPoints().add(y[0]+this.yCentre);
	     for (int i=1;i<nbCote;i++){
		   	x[i]=(double)(x[i-1]*Math.cos(a)-y[i-1]*Math.sin(a));
	    	y[i]=(double)(x[i-1]*Math.sin(a)+y[i-1]*Math.cos(a));
	    	this.getPoints().add(x[i]+xCentre);
		    this.getPoints().add(y[i]+yCentre);
		   }
	     Model.setListPoly(this);
	     final Path path=(Path) Path.union(new Rectangle(0,0),this);
	     path.setOnMouseClicked(e->Model.select(path));
	     path.setOnMouseEntered(e -> Tooltip.install(this, new Tooltip("nombre de cot�s = "+nbCote+"\nrayon = " +rayon/12.1)));
         path.setCursor(Cursor.HAND);
		 path.setFill(Color.color(Math.random(),Math.random(),Math.random(), 1));
		 Hexalearn.depSouris(path);
		 this.couleur=(Color) path.getFill();
		 DecimalFormat df = new DecimalFormat ( ) ;
		 df.setMaximumFractionDigits ( 1 ) ; //arrondi � 2 chiffres apres la virgules
		 df.setMinimumFractionDigits ( 1 ) ;
		 df.setDecimalSeparatorAlwaysShown ( true ) ;
		 path.setOnMouseEntered(e -> Tooltip.install(   path, new Tooltip("Pivot =(x = "+ (double)  ((int)((((path.getBoundsInParent().getMaxX()+path.getBoundsInParent().getMinX())/2-632.25)/10.08331298828125)*100))/100+", y = "+(double)((int)(((path.getBoundsInParent().getMaxY()+path.getBoundsInParent().getMinY())/2-355.5)/-9.69696044921875*100))/100+")")));
		 MainApp.groupe.getChildren().add(path);
		 Model.getListPoly().add(path);
	}

	/* attribuer a un polygone une trajectoire pour qu'il sera anim� sur ell **/
	public static boolean AnimateOnPath(double temps){
		double maxX,maxY,minX,minY;
		boolean bool=true;

		Alert alert = new Alert(AlertType.WARNING);
		if(getPath()==null)
		{
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
			alert.setHeaderText("Veuillez selectionner la trajectoire avant\nd'animer l'objet sur elle   !" );
			alert.setContentText("Veuillez r�essayer.");
			alert.showAndWait();
			bool=false;
		 }
		 else
		 {
			 if (temps==0)
			 {
				 MainApp.playSound("warning");
				 alert.setTitle("ATTENTION");
				 alert.setHeaderText("Veuillez selectionner le temps !" );
				 alert.setContentText("Veuillez r�essayer.");
				 alert.showAndWait();
			 }
			 else{
			   PathTransition pathTransition = new PathTransition(seconds(temps),getPath(),Model.objetComplex);
			   maxX = getPath().getBoundsInParent().getMaxX();
			   maxY = getPath().getBoundsInParent().getMaxY();
			   minX =  getPath().getBoundsInParent().getMinX();
			   minY =   getPath().getBoundsInParent().getMinY();
			   pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		       pathTransition.setAutoReverse(false);
		       double rayonx,rayony;
               rayonx=(Model.objetComplex.getBoundsInParent().getMaxX()-Model.objetComplex.getBoundsInParent().getMinX())/2;
               rayony=(Model.objetComplex.getBoundsInParent().getMaxY()-Model.objetComplex.getBoundsInParent().getMinY())/2;

               if (maxX+rayonx<1083.75&&minX-rayonx>230&&maxY+rayony<645&&minY-rayony>57){
            	   pathTransition.play();
            	   transition=pathTransition;
               }
               else
               {
            	   MainApp.playSound("warning");
            	 alert.setTitle("ATTENTION");
  		       	 alert.setHeaderText("L'animation sur cette trajectoire n'est pas possible\nsinon l'objet va sortir de la grille\ndans le cas de plusieurs trajectoire creer il faut \nselectionner une seule   !" );
  		       	 alert.setContentText("Veuillez r�essayer.");
  		       	 alert.showAndWait();
  		       	 bool=false;
               }
			 }
		 }
		return bool;
	}


}
