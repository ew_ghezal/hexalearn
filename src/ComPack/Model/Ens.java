package ComPack.Model;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

import ComPack.Controllers.WorkSpaceController;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import application.MainApp;

public class Ens extends Element{
	/**ajouter un compte enseignant **/
	public  void ajouter(String nom, String pw){
		try {
			pw=Model.toSHA1(pw.getBytes(StandardCharsets.UTF_8));
			this.nom=nom;
			String chemin="comptes/"+nom;
			new File(chemin).mkdirs();
			monFichier = new RandomAccessFile("f.txt", "rw");
			boolean trouv=Model.recherche(monFichier, nom);
			if(!trouv){
				monFichier.seek(monFichier.length());
				String enreg=nom+"#"+pw+"#ens"+"$";
				monFichier.writeUTF(enreg);
				WorkSpaceController.resett();
				MainApp.ShowWorkSpaceWindow();

			}
		}
		catch (Exception e) {e.printStackTrace();}
	}

	public static void seConnecter(String nom,String pw){
		try{
			Alert alert = new Alert(AlertType.WARNING);
			monFichier = new RandomAccessFile("f.txt", "rw");
			pw=Model.toSHA1(pw.getBytes(StandardCharsets.UTF_8));
			String chaine=nom+"#"+pw+"#ens"+"$";
			boolean trouv=Model.recherche(monFichier, chaine);
			if(trouv){
				MainApp.element.setNom(nom);
			}
			else {
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("Mot de passe ou nom d'utilisateur incorrect!" );
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();

			}
		}
		catch (Exception e){e.printStackTrace();}
		}
}