package ComPack.Model;

import static javafx.util.Duration.seconds;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import application.MainApp;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
/**La classe principale de notre application qui contient les différentes transformations et les vérifications **/
public abstract class Hexalearn {

	static Color col;
	static double orgSceneX;
	static double orgSceneY;
    static double orgTranslateX;
	static double orgTranslateY;
    static double offsetX ;
    static double offsetY ;
    static double newTranslateX ;
    static double newTranslateY ;
    static Path path = new Path();
    public static ArrayList<Object> temp=new ArrayList<Object>();

/**vérification si le déplacement par rapport a l'axe des abscisse est possible **/
    static public boolean deplacementX(Path polygone,double depx,Pane pane ){
		 polygone.setVisible(false);
		 double oldTranslate=polygone.getTranslateX();
		 polygone.setTranslateX(depx+polygone.getTranslateX());
		 double a,b;
		 a=polygone.getBoundsInParent().getMaxX();
	     b= polygone.getBoundsInParent().getMinX();

		 if (a<=1083.75 &&b>=230) {
			Polygone.setDepx(depx+polygone.getTranslateX());
			 polygone.setVisible(true);
			 return true;
		 }
		 else {
			 polygone.setTranslateX(oldTranslate);
			 polygone.setVisible(true);
			 return false;
		 }
	}

    /**vérification si le déplacement par rapport a l'axe des ordonnées est possible **/
	 static public boolean deplacementY(Path polygone,double depy,Pane pane ){

		 polygone.setVisible(false);
		 double oldTranslate=polygone.getTranslateY();
		 polygone.setTranslateY(depy+polygone.getTranslateY());
		 double a,b;
		 a=polygone.getBoundsInParent().getMaxY();
		 b=polygone.getBoundsInParent().getMinY();
		 if (a<=645&& b>=57) {
			 Polygone.setDepy(depy+polygone.getTranslateY());
			 polygone.setVisible(true);
			 return true;
		 }
		 else {
			 polygone.setTranslateY(oldTranslate);
			 polygone.setVisible(true);
			 return false;
		 }
	}

	 /**vérification si la redimension par une echelle est possible **/
	static public boolean redimension(Path polygone,double coef,Pane pane){
		polygone.setVisible(false);
		double oldScaleX=polygone.getScaleX();
		double oldScaleY=polygone.getScaleY();
		polygone.setScaleX(coef*polygone.getScaleX());
		polygone.setScaleY(coef*polygone.getScaleY());
		double a,b,c,d;
		 a=polygone.getBoundsInParent().getMaxX();
	     b= polygone.getBoundsInParent().getMinX();
	     c=polygone.getBoundsInParent().getMaxY();
		 d=polygone.getBoundsInParent().getMinY();

		  	if (a<=1083.75 && c<=645 && b>=230 && d>=57) {

			 Polygone.setCoefX(coef*polygone.getScaleX());
			 Polygone.setCoefY(coef*polygone.getScaleY());
			 polygone.setVisible(true);

			 return true;
		 }
		 else {
			 polygone.setScaleX(oldScaleX);
     		 polygone.setScaleY(oldScaleY);
			 polygone.setVisible(true);
			 return false;
		 }
	}

	/**vérification si la rotation  est possible **/

	static public boolean rotation(Path polygone,double angle,Pane pane ){
		polygone.setVisible(false);
		double a,b,c,d;
		double oldRotation=polygone.getRotate();
		a=polygone.getBoundsInParent().getMaxX();
		b= polygone.getBoundsInParent().getMinX();
		c=polygone.getBoundsInParent().getMaxY();
		d=polygone.getBoundsInParent().getMinY();
		if (a<=1083.75 && c<=645 && b>=230 && d>=57) {
			polygone.setVisible(true);
			polygone.setRotate(polygone.getRotate()+angle);
			return true;
		 }
		 else {
             polygone.setRotate(oldRotation);
			 polygone.setVisible(true);
			 return false;
		 }
	}

	public static void sauvegarder(Path path,double[] tab,File file,int code, double posx,double posy){

		final SauvComplexe enreg = new SauvComplexe();
		enreg.setPath(path.toString());
		ObjectOutputStream oos = null;
		enreg.setPosx(posx);
		enreg.setPosy(posy);
		enreg.setFromX(tab [0]);
		enreg.setFromY(tab [1]);
		enreg.setFromScale(tab [2]);
		enreg.setFromAngle(tab [3]);
		enreg.setToX(tab [4]);
		enreg.setToY(tab [5]);
		enreg.setToScale(tab [6]);
		enreg.setToAngle(tab [7]);
		enreg.setFromDuree(tab[8]);
		enreg.setToDuree(tab[9]);
		enreg.setFromBleu(tab[10]);
		enreg.setFromRouge(tab[11]);
		enreg.setFromVert(tab[12]);
		enreg.setToBleu(tab[13]);
		enreg.setToRouge(tab[14]);
		enreg.setToVert(tab[15]);
		enreg.setType("complexe");

		enreg.setCode(code);

		try {
			   FileOutputStream fichier = new FileOutputStream(file,true);
			   oos = new ObjectOutputStream(fichier);
			   oos.writeObject(enreg);
			   oos.flush();

	   } catch (final java.io.IOException e) {
		   e.printStackTrace();
	   } finally {
		   try {
			   if (oos != null) {
				   oos.flush();
				   oos.close();
			   }
		   } catch (final IOException ex) {
			   ex.printStackTrace();
		   }
	   	}
	}


	static public void sauvTraj(File file,String trajectoire, String polygone,double temps){
		ObjectOutputStream oos = null;
		final SauvPath enreg = new SauvPath();
		enreg.setTrajectoire(trajectoire);
		enreg.setPolygone(polygone);
		enreg.setTemps(temps);
		enreg.setType("trajectoire");
		try{
			FileOutputStream fichier = new FileOutputStream(file,true);
			   oos = new ObjectOutputStream(fichier);
			   oos.writeObject(enreg);
			   oos.flush();

	   } catch (final java.io.IOException e) {
		   e.printStackTrace();
	   } finally {
		   try {
			   if (oos != null) {
				   oos.flush();
				   oos.close();
			   }
		   } catch (final IOException ex) {
			   ex.printStackTrace();
		   }
	   	}
	 }

	public static void lire(File file,Timeline timeline,Pane pane){
		String tab[];

		int i=3;
		ObjectInputStream ois = null;

		SauvComplexe enreg;
		int code;
		path=new Path();
		try {
			final FileInputStream fichier = new FileInputStream(file);
			ois = new ObjectInputStream(fichier);
			enreg = (SauvComplexe)ois.readObject();
			tab=toPath(enreg.getPath(),pane);
			path.getElements().add(new MoveTo(Double.parseDouble(tab[1])+enreg.getPosx(),Double.parseDouble(tab[2])+enreg.getPosy()));
			while(tab[i].equals("LineTo"))
			{
				path.getElements().add(new LineTo(Double.parseDouble(tab[i+1])+enreg.getPosx(),Double.parseDouble(tab[i+2])+enreg.getPosy()));
				i+=3;
			}
			Color c = Color.web(tab[i]);
			path.setFill(c);
		   	MainApp.groupe.getChildren().add(path);
		   	temp.add(path);
		   	code=enreg.getCode();

			boolean check=true;
			while (check) {
				try{
					if (code!=enreg.getCode())
					   	{
						 	path= new Path();
						    tab=toPath(enreg.getPath(),pane);
						    path.getElements().add(new MoveTo(Double.parseDouble(tab[1])+enreg.getPosx(),Double.parseDouble(tab[2])+enreg.getPosy()));
							while(tab[i].equals("LineTo"))
							{
								path.getElements().add(new LineTo(Double.parseDouble(tab[i+1])+enreg.getPosx(),Double.parseDouble(tab[i+2])+enreg.getPosy()));
								i+=3;
							}
							c = Color.web(tab[i]);
							path.setFill(c);
							MainApp.groupe.getChildren().add(path);
							temp.add(path);
							code=enreg.getCode();
					}
			    final KeyValue initX = new KeyValue(path.layoutXProperty(),enreg.getFromX());
				final KeyValue initY = new KeyValue(path.layoutYProperty(),enreg.getFromY());
				final KeyValue initScaleX = new KeyValue(path.scaleXProperty(),enreg.getFromScale());
				final KeyValue initScaleY = new KeyValue(path.scaleYProperty(),enreg.getFromScale());
				final KeyValue initRotate = new KeyValue(path.rotateProperty(),enreg.getFromAngle());

				col=Color.color(enreg.getFromRouge(), enreg.getFromVert(), enreg.getFromBleu());
				final KeyValue initColor = new KeyValue(path.fillProperty(),col);

				final KeyFrame initkf = new KeyFrame(Duration.seconds(enreg.getFromDuree()),initX,initY,initScaleX,initScaleY,initRotate,initColor);

			    final KeyValue keyValueX = new KeyValue(path.layoutXProperty(),enreg.getToX());
			    final KeyValue keyValueY = new KeyValue(path.layoutYProperty(),enreg.getToY());
			    final KeyValue keyValueScaleX = new KeyValue(path.scaleXProperty(),enreg.getToScale());
			    final KeyValue keyValueScaleY = new KeyValue(path.scaleYProperty(),enreg.getToScale());
			    final KeyValue keyValueRotate = new KeyValue(path.rotateProperty(),enreg.getToAngle());

			    col=Color.color(enreg.getToRouge(), enreg.getToVert(), enreg.getToBleu());


			    final KeyValue keyValueColor = new KeyValue(path.fillProperty(),col);
			    final KeyFrame kf = new KeyFrame(Duration.seconds(enreg.getToDuree()), keyValueX,keyValueY,keyValueScaleX,keyValueScaleY,keyValueRotate,keyValueColor);
				timeline.getKeyFrames().addAll(kf,initkf);
				ois = new ObjectInputStream(fichier);
				enreg = (SauvComplexe)ois.readObject();
				i=3 ;

				} catch(EOFException ex){
					   check=false;
					   timeline.play();
					   timeline.setOnFinished(new EventHandler<ActionEvent>(){

						   @Override
						   public void handle(ActionEvent arg0) {

							   for (int i=0;i<temp.size();i++)
							   {
								   MainApp.groupe.getChildren().remove(temp.get(i));
							   }
							   temp.clear();
							   MainApp.groupe.getChildren().add(new Rectangle(0,0));
							   for (int i=0;i<Model.getList().size();i++)
								   if (Model.getList().get(i) instanceof Path){
									   MainApp.groupe.getChildren().add((Path) Model.getList().get(i));
									   Model.getListPoly().add((Path) Model.getList().get(i));
								   }
						   }
					   });

				}
			}
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			if (ois != null)
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

	}

	private static String[] toPath(String path,Pane pane){

		String[] tab = path.split(",|=|\\]|\\[|\\s");
		String[] pathelements=new String[tab.length];
		int j=0;

		for (int i=0; i<tab.length;i++){
			if (tab[i].equals("MoveTo")){
				pathelements[j]=tab[i];
				j++;
			}
			if (tab[i].equals("x")){
				pathelements[j]=tab[i+1];
				j++;
			}
			if (tab[i].equals("y")){
				pathelements[j]=tab[i+1];
				j++;
			}
			if (tab[i].equals("LineTo")){
				pathelements[j]=tab[i];
				j++;
			}
			if (tab[i].equals("fill")){
				pathelements[j]=tab[i+1];
				j++;
			}
		}
		return pathelements;
	}

	static public boolean deplacementX(double depx,Pane pane ){
		boolean bool;
		Model.objetComplex.setVisible(false);
		 double oldTranslate=Model.objetComplex.getTranslateX();
		 Model.objetComplex.setTranslateX(depx+Model.objetComplex.getTranslateX());

		 double a,b;
		 a=Model.objetComplex.getBoundsInParent().getMaxX();
	     b=Model.objetComplex.getBoundsInParent().getMinX();

		 if (a<=1083.75 &&b>=230) bool=true;
		 else bool=false;
		 Model.objetComplex.setTranslateX(oldTranslate);
		 Model.objetComplex.setVisible(true);
		 return bool;
	}

	static public boolean deplacementY(double depy,Pane pane ){
		boolean bool;
		Model.objetComplex.setVisible(false);
		 double oldTranslate=Model.objetComplex.getTranslateY();
		 Model.objetComplex.setTranslateY(depy+Model.objetComplex.getTranslateY());

		 double a,b;
		 a=Model.objetComplex.getBoundsInParent().getMaxY();
		 b=Model.objetComplex.getBoundsInParent().getMinY();
		 if (a<=645&& b>=57) bool= true;
		 else bool=false;
		 Model.objetComplex.setTranslateY(oldTranslate);
		 Model.objetComplex.setVisible(true);

		 return bool;
	}

	static public boolean redimension(double coef,Pane pane){
		boolean bool;
		Model.objetComplex.setVisible(false);
		double oldScaleX=Model.objetComplex.getScaleX();
		double oldScaleY=Model.objetComplex.getScaleY();
		Model.objetComplex.setScaleX(coef*Model.objetComplex.getScaleX());
		Model.objetComplex.setScaleY(coef*Model.objetComplex.getScaleY());
		double a,b,c,d;
		 a=Model.objetComplex.getBoundsInParent().getMaxX();
	     b= Model.objetComplex.getBoundsInParent().getMinX();
	     c=Model.objetComplex.getBoundsInParent().getMaxY();
		 d=Model.objetComplex.getBoundsInParent().getMinY();

		 if (a<=1083.75 && c<=645 && b>=230 && d>=57)  bool=true;
		 else bool=false;
		 Model.objetComplex.setScaleX(oldScaleX);
		 Model.objetComplex.setScaleY(oldScaleY);
		 Model.objetComplex.setVisible(true);

		return bool;
	}

	static public boolean rotation(double angle,Pane pane ){
		boolean bool;
		Model.objetComplex.setVisible(false);
		double a,b,c,d;
		double oldRotation=Model.objetComplex.getRotate();
		 a=Model.objetComplex.getBoundsInParent().getMaxX();
	     b= Model.objetComplex.getBoundsInParent().getMinX();
	     c=Model.objetComplex.getBoundsInParent().getMaxY();
		 d=Model.objetComplex.getBoundsInParent().getMinY();

		  	if (a<=1083.75 && c<=645 && b>=230 && d>=57)
			 bool=true;
		 else {
			 bool=false;
		 }

		 Model.objetComplex.setRotate(oldRotation);
		 Model.objetComplex.setVisible(true);
		return bool;
	}

	public static void lireTrajectoire(File file,Pane pane) {
		String tab[];
		ObjectInputStream ois = null;
		SauvPath enreg;
		Path trajectoire=new Path();
		int i=3;
		try {
			final FileInputStream fichier = new FileInputStream(file);
			ois = new ObjectInputStream(fichier);
			enreg = (SauvPath)ois.readObject();
			tab=toPath(enreg.getTrajectoire(),pane);
			trajectoire.getElements().add(new MoveTo(Double.parseDouble(tab[1]),Double.parseDouble(tab[2])));
			while(tab[i].equals("LineTo"))
			{
				trajectoire.getElements().add(new LineTo(Double.parseDouble(tab[i+1]),Double.parseDouble(tab[i+2])));
				i+=3;
			}
		   	MainApp.groupe.getChildren().add(trajectoire);
		   	temp.add(trajectoire);
		   	i=3;
		 	Path polygone= new Path();
			tab=toPath(enreg.getPolygone(),pane);
			polygone.getElements().add(new MoveTo(Double.parseDouble(tab[1])-224,Double.parseDouble(tab[2])-172));
			while(tab[i].equals("LineTo"))
			{
				polygone.getElements().add(new LineTo(Double.parseDouble(tab[i+1])-224,Double.parseDouble(tab[i+2])-172));
				i+=3;
			}
			Color 	c = Color.web(tab[i]);
			polygone.setFill(c);
			MainApp.groupe.getChildren().add(polygone);
			temp.add(polygone);
			PathTransition pathTransition = new PathTransition(seconds(enreg.getTemps()),trajectoire,polygone);
	        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
	        pathTransition.setAutoReverse(false);
	        pathTransition.play();
	        pathTransition.setOnFinished(new EventHandler<ActionEvent>(){
	        	@Override
	        	public void handle(ActionEvent arg0) {
	        		for (int i=0;i<temp.size();i++)
	        		{
	        			MainApp.groupe.getChildren().remove(temp.get(i));
	        		}
					temp.clear();
					MainApp.groupe.getChildren().add(new Rectangle(0,0));
	        		for (int i=0;i<Model.getList().size();i++)
	        			if (Model.getList().get(i) instanceof Path){
	        				MainApp.groupe.getChildren().add((Node) Model.getList().get(i));
	        				Model.getListPoly().add((Path) Model.getList().get(i));
	        			}
	        	}
	        });
		} catch(EOFException ex){

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			if (ois != null)
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}

	public static void CreateDefaultPoly(int nb_cotes){
		 MainApp.playSound("CreationPoly");
		 Polygone p=new Polygone(632.25,355.5,73,0,nb_cotes,Color.BLUE);
		 p.afficherPolygone();
	     p.setOnMouseEntered(e -> Tooltip.install(  p, new Tooltip("nombre de cotés = "+p.getNbCote()+"\nrayon = " + (p.getRayon()*p.getScale())/12.1)));
	}

	public static void depSouris(Path path){
		EventHandler<MouseEvent> shapeOnMousePressedEventHandler =
	             new EventHandler<MouseEvent>() {
	             @Override
	             public void handle(MouseEvent t) {
                    orgSceneX = t.getSceneX();
                    orgSceneY = t.getSceneY();
                    orgTranslateX = ((Shape)(t.getSource())).getTranslateX();
                    orgTranslateY = ((Shape)(t.getSource())).getTranslateY();
	             }
	        };

	        EventHandler<MouseEvent> shapeOnMouseDraggedEventHandler =
	        new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent t) {
           offsetX = t.getSceneX() - orgSceneX;
           offsetY = t.getSceneY() - orgSceneY;
           double a,b,c,d,oldTranslateX,oldTranslateY;

           offsetX = t.getSceneX() - orgSceneX;
           offsetY = t.getSceneY() - orgSceneY;
           newTranslateX = orgTranslateX + offsetX;
           newTranslateY = orgTranslateY + offsetY;
           ((Shape)(t.getSource())).setVisible(false);
           oldTranslateX= ((Shape)(t.getSource())).getTranslateX();
           oldTranslateY= ((Shape)(t.getSource())).getTranslateY();
          ((Shape)(t.getSource())).setTranslateX(newTranslateX);
          ((Shape)(t.getSource())).setTranslateY(newTranslateY);
          a=(((Shape)(t.getSource())).getBoundsInParent().getMaxX());
          b= ((Shape)(t.getSource())).getBoundsInParent().getMinX();
          c= ((Shape)(t.getSource())).getBoundsInParent().getMaxY();
          d=((Shape)(t.getSource())).getBoundsInParent().getMinY();
          if (a <=1068&&b >=232.75&&c<=652.5 &&d >=53.5)
          {
         	  ((Shape)(t.getSource())).setTranslateX(newTranslateX);
               ((Shape)(t.getSource())).setTranslateY(newTranslateY);
              ((Shape)(t.getSource())).setVisible(true);
           }
           else
          {
         	 ((Shape)(t.getSource())).setTranslateX(oldTranslateX);
              ((Shape)(t.getSource())).setTranslateY(oldTranslateY);
              ((Shape)(t.getSource())).setVisible(true);
          }
	       }
	     };

       path.setOnMousePressed(shapeOnMousePressedEventHandler);
       path.setOnMouseDragged(shapeOnMouseDraggedEventHandler);
	}
}