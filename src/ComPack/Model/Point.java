package ComPack.Model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;


public class Point {
	  private DoubleProperty x=new SimpleDoubleProperty(this,"x",0.0);
	  private DoubleProperty y=new SimpleDoubleProperty(this,"y",0.0);

	  /*getters and setters*/
	  public final double getX(){
		  return this.x.get();
	  }
	  public final void setX(double x){
		  this.x.set(x);
	  }
	  public final double getY(){
		  return this.y.get();
	  }
	  public final void setY(double y){
		  this.y.set(y);
	  }
	  //double x,y;

}
