package ComPack.Model;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import application.MainApp;

public class Trajectoire {
	public Path path;
	public static Path path1;

/*cette methode nous donne la main pour dessiner une trajectoire avec la souris*/

	public void trajectoire() {
		this.path=new Path();
		path1=new Path();
		this.path.setStrokeWidth(2.5);
		this.path.setStroke(Color.DARKBLUE);
		path1=this.path;
		Alert alert = new Alert(AlertType.WARNING);
	        MainApp.sceneWS.onMousePressedProperty().set(new EventHandler<MouseEvent>(){
	            @Override
	            public void handle(MouseEvent event) {
	            	if ((event.getX()>=230)&(event.getX()<=1083.75))
	            		if ((event.getY()>=57)&(event.getY()<=645))
	            			path1.getElements().add(new MoveTo(event.getX(), event.getY()));
	            		else{
	            			MainApp.playSound("warning");
	            			 alert.setTitle("ATTENTION");
	        		       	 alert.setHeaderText("Il faut dessiner la trajectoire a l'interieur de la grille  !" );
	        		       	 alert.setContentText("Veuillez r�essayer.");
	        		       	 alert.showAndWait();
	            		}
	            }
	        });

	        MainApp.sceneWS.onMouseDraggedProperty().set(new EventHandler<MouseEvent>(){
	            @Override
	            public void handle(MouseEvent event) {
	            	if ((event.getX()>=240)&(event.getX()<=1070))
	            		if ((event.getY()>=68)&(event.getY()<=635))
	            			path1.getElements().add(new LineTo(event.getX(), event.getY()));
	            }
	        });
	        this.path=path1;
	        MainApp.sceneWS.onMouseReleasedProperty().set(new EventHandler<MouseEvent>(){
	            @Override
	            public void handle(MouseEvent event) {
	            	MainApp.sceneWS.onMousePressedProperty().set(null);
	            	MainApp.sceneWS.onMouseDraggedProperty().set(null);
	            }
	        });
	        this.path.setOnMouseClicked(e-> Model.selectT(this.path));
	        MainApp.groupe.getChildren().add(this.path);
	        Model.poly.add(this.path);
	        this.path.setCursor(Cursor.HAND);
	}
}
