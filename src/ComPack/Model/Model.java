/** La classe qui contient l'objet polygone et l'objet complexe ainsi que leur
 *   methode et attributs(le centre , le nombre de cot�s ect..)**/


package ComPack.Model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import ComPack.Controllers.WorkSpaceController;

public class Model{

	public static Path objetComplex=new Path(),prev;
	public static Shape shape=new Rectangle(0,0);
	public static Polygone p=new Polygone();
	public static ArrayList<Object> poly=new ArrayList<Object>();
	public static ArrayList<Object> list=new ArrayList<Object>();
	public static ArrayList<Object> getList() {
		return list;
	}
	public static void setList(ArrayList<Object> list) {
		Model.list = list;
	}

	public static   Path path;
    public static double oldDepX;
    public static double oldDepY;
    public static double oldRotation;
    public static double oldRedimension;

/** le tableau dynamique qui contient tout les polygone creer **/
	public static void setListPoly(Object p){
		poly.add(p);
	}
	public static ArrayList<Object> getListPoly(){
		return poly;
	}
/** la fonctions qui selectionne un polygone ou un objet complexe pour pouvoir le manipuler **/
	public static void select(Path path){
		if (Model.path!=null)
		{
			Model.path.setStrokeWidth(2.5);
			Model.path.setStroke(Color.DARKBLUE);
		}
		prev=objetComplex;
		objetComplex.setStroke(null);
		path.setStrokeWidth(3);
		objetComplex=path;
		path.setStroke(Color.BLACK);
		WorkSpaceController.duree=0;
		WorkSpaceController.n=0;
	}
	/** la fonction qui selectionne une trajectoire **/
	public static void selectT(Path tj){
		Model.objetComplex.setStroke(null);
		if (Model.path!=null)
		{
			Model.path.setStrokeWidth(2.5);
			Model.path.setStroke(Color.DARKBLUE);
		}
		Model.path=tj;
		tj.setStroke(Color.GREEN);
		tj.setStrokeWidth(5);
		Model.p.setPath(Model.path);
	}
/** Afficher le message de confirmation si on veut sortir du programme **/
	public static String toSHA1(byte[] convertme) {
	    MessageDigest md = null;
	    try {
	        md = MessageDigest.getInstance("SHA-1");
	    }
	    catch(NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return new String(md.digest(convertme));
	}

	/**Rechercher un compte sur le fichier si on ajoute un compte pour v�rifier si il existe deja **/
	public static boolean recherche(RandomAccessFile f,String chaine){
		boolean trouv=false;
		try{
			 if(f.length()!=0){
				 String data=f.readUTF();
					while((!trouv)&&(f.getFilePointer()<=f.length()))
					{
						if(data.contains(chaine)){
							trouv=true;
						}
						else{
							if(f.getFilePointer()!=f.length()) data=f.readUTF();
							else break;
						}
					}
			 }

		}catch(Exception e){
			e.printStackTrace();
		}
		return trouv;
	}

	/**V�rifier si le nom d'utilisateur contient que des caracteres alphab�tiques **/
	public static boolean isAlpha(String name) {
	    char[] chars = name.toCharArray();
	    for (char c : chars) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }
	    return true;
	}

	/**copier un fichier **/
	public static void copyFile(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}

}