package ComPack.Controllers;

import java.awt.Desktop;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.util.Duration;
import ComPack.Model.Element;
import ComPack.Model.Eleve;
import ComPack.Model.Ens;
import ComPack.Model.Model;
import application.MainApp;

public class AcceuilController {


	public static int nbAppelWorkSpaceStage = 0;
	public static ImageView profilePic = new ImageView();
	public ImageView check;
	public FadeTransition fd;
	SequentialTransition seq = new SequentialTransition();

	//Declarations des objets graphiques ins�r�s dans la fenetre d'acceuil
	@FXML
	ImageView loginIcon;

	@FXML
	GridPane noms;

	@FXML
	Button hexalearn;

	@FXML
	private ProgressIndicator pI;

	@FXML
	private ProgressBar pB;

	@FXML
	private TabPane tabPane;

	@FXML
	AnchorPane ajouterPane;

	@FXML
	Pane aProposPane;

	@FXML
	Pane aidePane;

	@FXML
	private TextField nom;

	@FXML
	private Side entrainement;

	@FXML
	AnchorPane loginPane;

	@FXML
	Pane hexaPane;

	@FXML
	private TextField nomUtilisateur;

	@FXML
	private Button ajouter, oui, non;

	@FXML
	private TextField prenom;

	@FXML
	ImageView logo;

	@FXML
	private TextField motDePasse;

	@FXML
	public PasswordField pf;

	@FXML
	public TextField txtUserName;

	@FXML
	public static Button btnLogin = new Button("Entrer");
	@FXML
	private RadioButton connectEleve = new RadioButton();
	@FXML
	private RadioButton connectEns = new RadioButton();
	@FXML
	Text h, h1, h2, h3, h4, h5, h6, h7, h8, hexa, equipe, annee,rem,text,t1,t2,t3;
	@FXML
	ImageView image;
	@FXML
	private RadioButton creerEleve = new RadioButton();
	@FXML
	private RadioButton creerEns = new RadioButton();

	/*Action d'affichage d'aide a propos de Creation*/
	@FXML
	void creation1() {
		MainApp.ShowAideEnLigne1();
	}
	/*Action d'affichage du tutoriel sur la creation d un objet complexe*/
	@FXML
	void creation2() {
		MainApp.ShowAideEnLigne2();
	}
	/*Action d'affichage du tutoriel sur comment appliquer des deffirente transformations*/

	@FXML
	void creation3() {
		MainApp.ShowAideEnLigne3();
	}
	/*Action d'affichage 'aide* sur la creation d'animation*/
	@FXML
	void creation4() {
		MainApp.ShowAideEnLigne4();
	}
	/*Action d'affichage d'aide sur animation sur une trajectoire*/
	@FXML
	void creation5() {
		MainApp.ShowAideEnLigne5();
	}
	/*Action d'affichage d'aide sur la sauvegarde et l'enregistrement*/
	@FXML
	void creation6() {
		MainApp.ShowAideEnLigne6();
	}

	/* bouton d'affichage de l'espace de travail dans le cas d'une utilisation sans compte*/
	/* action du bouton "Entrainement"*/
	public void ActionBtnEntrainement() {
		tabPane.getSelectionModel().select(1);
		if (nbAppelWorkSpaceStage == 0) {
			MainApp.ShowWorkSpaceWindow();// on appele cette une fois seulement
			nbAppelWorkSpaceStage++;
		} else
			MainApp.AcceuilStage.hide();
		MainApp.WorkSpaceStage.show();
	}

	/* action du bouton "Se connecter"*/
	public void ActionBtnSeConnecter() {
		tabPane.getSelectionModel().select(2);
		fd = new FadeTransition(Duration.millis(1000), loginPane);
		fd.setFromValue(0);
		fd.setToValue(1);
		fd.play();
	}
	/* action du bouton "Quitter"*/
	public void ActionBtnQuitter() {
		tabPane.getSelectionModel().select(6);
	}
 // fermeture de l'application dans le cas on clique sur oui pour quiter
	@FXML
	public void close() {
		MainApp.AcceuilStage.close();
		MainApp.WorkSpaceStage.close();
	}
  // si on clique sur non(on va pa quitter):
	@FXML
	public void nonClose() {
		tabPane.getSelectionModel().select(0);
	}
	/* action du bouton "Ajouter compte"*/
	public void ActionBtnAjouterCompte() {
		fd = new FadeTransition(Duration.millis(1000), ajouterPane);
		fd.setFromValue(0);
		fd.setToValue(1);
		fd.play();
		tabPane.getSelectionModel().select(3);
	}
	/* action du bouton "A propos"*/
	public void ActionBtnApropos() {

 	    tabPane.getSelectionModel().select(4);
 	    fd=new FadeTransition(Duration.millis(1000),aProposPane);
		fd.setFromValue(0);
		fd.setToValue(1);
		fd.play();
    	TranslateTransition t=new TranslateTransition(Duration.millis(4000),logo);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),hexa);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),equipe);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),noms);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),rem);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),text);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),t1);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),t2);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();
    	t=new TranslateTransition(Duration.millis(4000),t3);
    	t.setFromY(tabPane.getHeight());
    	t.setToY(0);
    	t.play();

	}
	/* action du bouton "Aide"*/
	public void ActionBtnAide() {
		fd = new FadeTransition(Duration.millis(1000), aidePane);
		fd.setFromValue(0);
		fd.setToValue(1);
		fd.play();
		tabPane.getSelectionModel().select(5);
	}
	/* action du bouton "Se connecter" apres avoir saisir le nom et le mot de passe*/
	public void btnLogin() throws IOException {

		Alert alert = new Alert(AlertType.WARNING);
		if (!connectEleve.isSelected() && !connectEns.isSelected()) {
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
			alert.setHeaderText("Veuillez selectionner Enseignant ou Eleve avant d'ajouter  !!");
			alert.setContentText("Veuillez r�essayer.");
			alert.showAndWait();
		} else if (connectEleve.isSelected()) {
			MainApp.element = new Eleve();
			Element.seConnecter(txtUserName.getText(), pf.getText());
		} else {
			MainApp.element = new Ens();
			Ens.seConnecter(txtUserName.getText(), pf.getText());
		}
	}
/* ses deux fonctions sert a eviter la selection du "Ens" ou "Eleve" au mm temps lors d une connection*/
	public void connectEleve() {
		if (connectEleve.isSelected()) {
			connectEns.setSelected(false);
		} else {
			connectEns.setSelected(true);
		}
	}

	public void connectEns() {
		if (connectEns.isSelected()) {
			connectEleve.setSelected(false);
		} else {
			connectEleve.setSelected(true);
		}
	}

	/* action du bouton "Bienvenue"*/
	public void ActionBtnBienvenue() {
		TranslateTransition s = new TranslateTransition(Duration.millis(4000),
				image);
		FadeTransition text = new FadeTransition(Duration.millis(500), h);
		FadeTransition text1 = new FadeTransition(Duration.millis(500), h1);
		FadeTransition text2 = new FadeTransition(Duration.millis(500), h2);
		FadeTransition text3 = new FadeTransition(Duration.millis(500), h3);
		FadeTransition text4 = new FadeTransition(Duration.millis(500), h4);
		FadeTransition text5 = new FadeTransition(Duration.millis(500), h5);
		FadeTransition text6 = new FadeTransition(Duration.millis(500), h6);
		FadeTransition text7 = new FadeTransition(Duration.millis(500), h7);
		FadeTransition text8 = new FadeTransition(Duration.millis(500), h8);
		SequentialTransition seq = new SequentialTransition();
		tabPane.getSelectionModel().select(0);
		text.setFromValue(0);
		text.setToValue(1);
		text1.setFromValue(0);
		text1.setToValue(1);
		text2.setFromValue(0);
		text2.setToValue(1);
		text3.setFromValue(0);
		text3.setToValue(1);
		text4.setFromValue(0);
		text4.setToValue(1);
		text5.setFromValue(0);
		text5.setToValue(1);
		text6.setFromValue(0);
		text6.setToValue(1);
		text7.setFromValue(0);
		text7.setToValue(1);
		text8.setFromValue(0);
		text8.setToValue(1);

		s.setAutoReverse(false);
		s.setCycleCount(1);
		s.setFromX(0);
		s.setToX(377);
		s.play();
		seq.setCycleCount(1);
		seq.getChildren().addAll(text, text1, text2, text3, text4, text5,
				text6, text7, text8);
		seq.setInterpolator(Interpolator.LINEAR);
		seq.play();

	}
	/* action du bouton "Ajouter" qui se trouve apres avoir saisir les information pour ajouter un nouveau compte*/
	public void ajouterElem() {
		Alert alert = new Alert(AlertType.WARNING);
		if (!creerEleve.isSelected() && !creerEns.isSelected()) {
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
			alert.setHeaderText("Veuillez selectionner Enseignant ou Eleve avant d'ajouter  !!");
			alert.setContentText("Veuillez r�essayer.");
			alert.showAndWait();
		} else {
			if (creerEleve.isSelected()) {
				MainApp.element = new Eleve();
			} else {
				MainApp.element = new Ens();
			}
			Element e = MainApp.element;
			if (Model.isAlpha(nom.getText())) {
				if (motDePasse.getText().length() < 6) {
					MainApp.playSound("warning");
					alert.setTitle("ATTENTION");
					alert.setHeaderText("Veuillez saisir un mot de passe de 6 caract�res ou plus  !!");
					alert.setContentText("Veuillez r�essayer.");
					alert.showAndWait();
				} else {
					RandomAccessFile file;
					try {
						file = new RandomAccessFile("f.txt", "rw");
						boolean exist = Model.recherche(file, nom.getText());
						if (exist) {
							MainApp.playSound("warning");
							alert.setTitle("ATTENTION");
							alert.setHeaderText("Le compte portant ce nom d'utilisateur existe d�j� !!");
							alert.setContentText("Veuillez r�essayer.");
							alert.showAndWait();
						} else
							e.ajouter(nom.getText(), motDePasse.getText());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

			} else {
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("Le nom d'utilisateur ne doit contenir \nque des caract�res alphab�tiques !!");
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();
			}
			if (nom.getText().length() == 0) {
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("Veuillez saisir que des caracteres alphab�tiques le champ NOM !!");
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();
			}
		}
	}
	/* ses deux fonctions sert a eviter la selection du "Ens" ou "Eleve" au mm temps lors d'ajout d'un compte*/

	public void creerEleve() {
		if (creerEleve.isSelected()) {
			creerEns.setSelected(false);
		} else {
			creerEns.setSelected(true);
		}
	}

	public void creerEns() {
		if (creerEns.isSelected()) {
			creerEleve.setSelected(false);
		} else {
			creerEleve.setSelected(true);
		}
	}
/*arreter l animation du nom de l'application dans "Bienvenue"*/
	@FXML
	public void hexaFade() {
		seq.stop();
	}
/*/* action du bouton "page aide" qui affiche une page web sur l'application*/
	public void aide() {
		try {
			Desktop.getDesktop().browse(new URI("page.htm"));
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
	}

}