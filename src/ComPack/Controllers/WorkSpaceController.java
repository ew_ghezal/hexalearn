
/**La classe qui est le lien entre l'interface graphique et la gestion
 *                      des �venements  **/




package ComPack.Controllers;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Duration;
import ComPack.Model.Hexalearn;
import ComPack.Model.Model;
import ComPack.Model.Polygone;
import ComPack.Model.SauvComplexe;
import ComPack.Model.SauvPath;
import ComPack.Model.Trajectoire;
import application.MainApp;


public class WorkSpaceController implements Initializable {

	 private int nb_cotes = 0;
	 private Color clr;
	 private FileChooser fc= new FileChooser();
	 private File file;
	 public static int n=0,cpt=0;
	 public static double duree=0,temps=0,rayon=0,posx=0, posy=0;
	 static double tab[]=new double [16];
	 static  boolean modif=false,bool=false;
	 static Timeline timeline=new Timeline();
	 static Timeline timeline2=new Timeline();
	 static Path pa=new Path();
	 static File selectedFile;

	 @FXML
		ImageView son;

	 @FXML
	 Text currentRateText;

	 @FXML
	 Button supp;

	 @FXML
	 Button reset;

	 @FXML
	 NumberAxis axeX;

	 @FXML
	 ProgressBar barre;

	 @FXML
	 private ToggleButton tb;

	 @FXML
	 public ToggleButton regrouper;

	 @FXML
	 private RadioButton play;

	 @FXML
	 Pane pane;

	 @FXML
	 private TextField depx;

	 @FXML
	 private TextField depy;

	 @FXML
	 private TextField echelle;

	 @FXML
	 private TextField angle;

	 @FXML
	 ToggleButton enreg;

	 @FXML
	 private Slider axeTemps;

	 @FXML
	 private TextField field;

	 @FXML
	 Button arreter;

	 @FXML
	 private TabPane tabPaneW ;

	 @FXML
	 Button stop;

	 @FXML
	 Button pause;

	 @FXML
	 private ColorPicker couleur;

	 @FXML
	 private Button sauvegarder;

	 @FXML
	 private Button dessiner;

	 @FXML
	 private Button ouvrir;

	 @FXML
	 private Button objetComplex;

	 @FXML
	 private Button trajectoire;

	 @FXML
	 private TitledPane rotation;

	 @FXML
	 private TitledPane deplacement;

	 @FXML
	 private TitledPane echelle1;

	 @FXML
	 private Button carree;

	 @FXML
	 private Button triangle;

	 @FXML
	 public ImageView profilePic;

	 @FXML
	 private Button hexagone;

	 @FXML
	 private Button creer;

	 @FXML
	 private Button returnHome;

	 @FXML
	 private TextField nb_cot�s;

	 @FXML
	 TextField cote;

	 @FXML
	 TextField scale;

	 @FXML
	 TextField rotate;


	 /**Creer un polygone avec parametre(rayon,nombre de cot�s , et l'angle)**/

	 @FXML
	 private void ClickCreer() {
		 boolean creer=true;
		 double rayon=0;
		 int nb_cotes = 0;
		 double angle=0;

		 Alert alert = new Alert(AlertType.WARNING);
		 try{
			 rayon=Double.parseDouble(scale.getText())*12.1;
			 if ((rayon/12.1  <= 0) || (rayon/12.1 > 15)) throw new Exception() ;
		 } catch (NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le rayon doit �tre un entier superieur � 0 et inf�rieur � 15 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 catch (Exception e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le rayon doit �tre un entier superieur � 0 et inf�rieur � 15 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 try{
			 nb_cotes=(int) Double.parseDouble(cote.getText());
			 if (nb_cotes  <= 2|| nb_cotes>50) throw new Exception();
		 } catch (NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le nombre de c�t�s doit �tre un entier sup�reur � 2 et inferieur � 50 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 catch (Exception e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le nombre de c�t�s doit �tre un entier sup�reur � 2 et inferieur � 50 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 try{
			 angle=Double.parseDouble(rotate.getText());
		 } catch (NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("L'angle doit �tre un nombre entier ou flottant !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 if(creer==true)
		 {
			 Polygone poly=new Polygone(632.25,355.5,rayon,angle,nb_cotes,Color.BLUE);
			 poly.afficherPolygone();
		 }
	 }

/**Dessiner un polygone**/

	 @FXML
	 private void ClickOk()
	 {
		 boolean creer=true;
		 Polygone p=new Polygone();
		 Alert alert = new Alert(AlertType.WARNING);
		 try
		 {
			 nb_cotes=(int) Double.parseDouble(nb_cot�s.getText());
			 if (nb_cotes  <= 2|| nb_cotes>50) throw new Exception();
		 }
		 catch (NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le nombre de c�t�s doit �tre un entier sup�reur � 2 et inferieur � 50 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 catch (Exception e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le nombre de c�t�s doit �tre un entier sup�reur � 2 et inferieur � 50 !" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 if(creer)
		 {
			Circle circle=new Circle();
			EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
		    int count = 0;
		    double p1,p2;
		 	boolean drawShape = true,circleCreer=false;
		 	double centrex ;
		 	double centrey ;
		 	boolean removeCircle=false;
	 	    double y[] = new double[nb_cotes];
	 	    double x[] = new double[nb_cotes];
	 	    double e,f;
	 	    double rayon;
	 	    Alert alert = new Alert(AlertType.WARNING);
	 	    @Override
	 	    public void handle(MouseEvent mouseEvent) {
	 		   if (mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED) {
	 			   if (drawShape) {
	                    x[count] = mouseEvent.getX();
	                    y[count] = mouseEvent.getY();
	                    centrex=x[0];
	                    centrey=y[0];
	                    circle.setCenterX(x[0]);
	                    circle.setCenterY(y[0]);
	                    circle.setFill(Color.RED);
	                    circle.setRadius(4);
			       }
			   } else if (mouseEvent.getEventType() == MouseEvent.MOUSE_RELEASED) {
				      e= mouseEvent.getSceneX();
				      f= mouseEvent.getSceneY();
				      if (drawShape) {
				    	  if (232.75<e&&e<1068&&53.5<f&&f<652.5)
				    	  {
				    		  count++;
				    		  if(count==2)
				    		  {
				    			  p1=Math.abs(x[1]-x[0]);
				    			  p2=Math.abs(y[1]-y[0]);
				    			  rayon=Math.sqrt(p1*p1+p2*p2);
				    			  if (x[0]+rayon>1068||x[0]-rayon<232.75||y[0]+rayon>652.5||y[0]-rayon<53.5)
				    			  {
				    				  count--;
				    				  MainApp.playSound("warning");
				    				  alert.setTitle("ATTENTION");
				    				  alert.setHeaderText("Le rayon saisi est trop grand le polygone ne peut pas �tre cr�� !!" );
				    				  alert.setContentText("Veuillez r�essayer.");
				    				  alert.showAndWait();
				    			  }
				    		  }
				    	  }
				    	  else
				    	  {
				    		  MainApp.playSound("warning");
				    		  alert.setTitle("ATTENTION");
				    		  alert.setHeaderText("Veuillez cliquer � l'interieur de la grille\npour dessiner votre polygone !!" );
				    		  alert.setContentText("Veuillez r�essayer.");
				    		  alert.showAndWait();
				    		  if (count==0)
				    			  removeCircle=true;
				    	  }
				    	  if (count==1&&circleCreer==false)
				    	  {
	                		MainApp.groupe.getChildren().add(circle);
				    	  }
				    	  else if(count==1&&removeCircle==true)
				    	  {
	                		MainApp.groupe.getChildren().add(circle);
				    	  }
				    	  circleCreer=true;
	                }
			        if (count == 2) {
			        	MainApp.playSound("CreationPoly");
			            MainApp.groupe.getChildren().remove(circle);
			            drawShape = false;
			            count = 0;
			            double a=(360/nb_cotes),b;
			            double tabx[]=new double[nb_cotes];
			            double taby[]=new double[nb_cotes];
			            a =Math.toRadians(a);
			            tabx[0]=x[1]-x[0];
			            taby[0]=y[1]-y[0];
			            b=Math.toDegrees((Math.acos(p1/rayon)));
			            p.setAngle(b);
			   	     	p.getPoints().add(tabx[0]+centrex);
			   	     	p.getPoints().add(taby[0]+centrey);
			   	     	for (int i=1;i<nb_cotes;i++){
			   	     		tabx[i]=(double)(tabx[i-1]*Math.cos(a)-taby[i-1]*Math.sin(a));
			   	     		taby[i]=(double)(tabx[i-1]*Math.sin(a)+taby[i-1]*Math.cos(a));
			   	     		p.getPoints().add(tabx[i]+centrex);
			   	     		p.getPoints().add(taby[i]+centrey);
			   	     	}
			   	     	p.setFill(Color.BLUE);
			   	     	p.setNbCote(nb_cotes);
			   	     	p.setRayon(rayon);
			   	     	final Path path=(Path) Path.union(new Rectangle(0,0),p);
						path.setFill(Color.color(Math.random(),Math.random(),Math.random(),1));
						path.setOnMouseClicked(e-> Model.select(path));
						 Hexalearn.depSouris(path);
						DecimalFormat df = new DecimalFormat ( ) ;
						df.setMaximumFractionDigits ( 1 ) ; //arrondi � 2 chiffres apres la virgules
						df.setMinimumFractionDigits ( 1 ) ;
					 	df.setDecimalSeparatorAlwaysShown ( true ) ;
					 	path.setOnMouseEntered(e -> Tooltip.install(   path, new Tooltip("Pivot =(x = "+ (double)  ((int)((((path.getBoundsInParent().getMaxX()+path.getBoundsInParent().getMinX())/2-632.25)/10.08331298828125)*100))/100+", y = "+(double)((int)(((path.getBoundsInParent().getMaxY()+path.getBoundsInParent().getMinY())/2-355.5)/-9.69696044921875*100))/100+")")));
						MainApp.groupe.getChildren().add(path);
						Model.getListPoly().add(path);
			        	}
			   		}
	 	    	}
			};
			MainApp.groupe.setOnMouseDragged(mouseHandler);
			MainApp.groupe.setOnMousePressed(mouseHandler);
			MainApp.groupe.setOnMouseReleased(mouseHandler);
		 }
	 }

	@FXML
	private void handleNewPolygone() {
		posx=626.75;
		posy=354.5;
	}

/**Les fonctions showToolTip sont pour l'aide interactif ou la souris et sur un bouton on affiche la fonction de ce dernier **/
	@FXML
	public void showToolTipSauvegarder(){
		Tooltip.install(sauvegarder, new Tooltip("Sauvegarder une animation sur un fichier "));
	}

	@FXML
	public void showToolTipDessiner(){
		Tooltip.install(dessiner, new Tooltip("Dessiner un polygone avec\nla souris "));
	}

    @FXML
    void showToolTipEnreg() {
    	Tooltip.install(enreg, new Tooltip("Enregistrer une animation"));
    }

    @FXML
    void showToolTipObjetComplex() {
    	Tooltip.install(objetComplex, new Tooltip("Regrouper 2 objet ou plus pour\ncreer un objet complex"));
    }

    @FXML
    void showToolTipOuvrir() {
    	Tooltip.install(ouvrir, new Tooltip("Ouvrir une animation deja enregistrer"));
    }

    @FXML
    void showToolTipSupp() {
    	Tooltip.install(supp, new Tooltip("Supprimer l'objet selectionn� "));
    }

    @FXML
    void showToolTipReset() {
    	Tooltip.install(reset, new Tooltip("Supprimer Tous les objets de la scene "));
    }

    @FXML
    void showToolTipPause() {
    	Tooltip.install(pause, new Tooltip("Faire une pause a l'animation"));
    }

    @FXML
    void showToolTipArreter() {
		Tooltip.install(arreter, new Tooltip("Arreter l'animation courante"));
    }

    @FXML
    void showToolTipPlay() {
    	Tooltip.install(play, new Tooltip("Voir l'animation courante"));
    }

    @FXML
    void showToolTipReturnHome() {
    	Tooltip.install(returnHome, new Tooltip("Retourner a la page d'accuil"));
    }

    @FXML
    void showToolTipTrajectoire() {
    	Tooltip.install(trajectoire, new Tooltip("Voir l'animation courante de objet complex ou le polygone\nsur une trajectoire"));
    }

    @FXML
    void showToolTipStop() {
    	Tooltip.install(stop, new Tooltip("Stoper l'annimation "));
    }

    @FXML
    void showToolTipEchelle() {
    	Tooltip.install(echelle1, new Tooltip("Agrandir ou Apetiiser le polygone/objet complex"));
    }

    @FXML
    void showToolTipRotation() {
    	Tooltip.install(rotation, new Tooltip("faire tourner un polygone/objet complex"));
    }

    @FXML
    void showToolTipDeplacement() {
    	Tooltip.install(deplacement, new Tooltip("deplacer un polyone/objet complex "));
    }

    @FXML
    void showToolTipCouleur() {
    	Tooltip.install(couleur, new Tooltip("changer la couleur du polygone/objet complex"));
    }

    @FXML
    void showToolTipCarre() {
    	Tooltip.install(carree, new Tooltip("Creer un carr� par defaut"));
    }

    @FXML
    void showToolTipTriangle() {
    	Tooltip.install(triangle, new Tooltip("Creer un triangle par defaut"));
    }

    @FXML
    void showToolTipHexagone() {
    	Tooltip.install(hexagone, new Tooltip("Creer un hexagone par defaut"));
    }

    @FXML
    void showToolTipCreer() {
    	Tooltip.install(creer, new Tooltip("Creer un polygone en entrant ses parametres "));
    }

    /**creer un triangle par d�faut**/
	@FXML
	private void CreateTriagle(){
		 Hexalearn.CreateDefaultPoly(3);
	}

	 /**creer un carr� par d�faut**/
	@FXML
	private void CreateCarre(){
		 Hexalearn.CreateDefaultPoly(4);
	}

	/**creer un octagone par d�faut **/
	@FXML
	private void CreateOctagone(){
		 Hexalearn.CreateDefaultPoly(6);
	}

	/**d�placer l'objet par rapport a l'axe des abscisses **/
	@FXML
	public void DeplacementX() {
		Alert alert = new Alert(AlertType.WARNING);
		boolean bool,creer=true;
		depx.setEditable(true);
		double depX = 0;
		try
		{
			depX=(Double.parseDouble(depx.getText())*12.1)/6*5;
		}
		catch (NumberFormatException e)
		{
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
	       	alert.setHeaderText("Le deplacement par rapport � l'axe des abscisses doit �tre un flotant ou un entier !!" );
	       	alert.setContentText("Veuillez r�essayer.");
	       	alert.showAndWait();
	       	creer=false;
		}
		if(creer && !modif)
		{
			bool= Hexalearn.deplacementX(Model.objetComplex,depX, pane);
			if(bool)
				Model.oldDepX=depX;
			else
				Model.oldDepX=0;
			if (!bool&&Model.objetComplex.getTranslateX()!=0)
			{
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("Le deplacement par rapport a l'axe des abscisses de "+depx.getText()+" de votre objet n'est pas possible \nsinon votre objet va sortir de la grille !!" );
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();
			}

			else if(Model.objetComplex.getTranslateX()==0&& Model.oldDepX==0)
			{
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("Veuillez selectionner l'objet avant \nde le deplacer sur l'axe des abscisses  !!" );
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();
			}
		}
	}

	/**d�placer l'objet par rapport a l'axe des ordonn�es **/
	 @FXML
	 public void DeplacementY() {
		 Alert alert = new Alert(AlertType.WARNING);
		 boolean bool,creer=true;
		 depy.setEditable(true);
		 double depY=0;
		 try
			{
			 depY=(Double.parseDouble(depy.getText())*-6.4)/3.30*5;
			}
		 catch(NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
			 alert.setHeaderText("Le deplacement par rapport � l'axe des ordonn�es doit �tre un flotant ou un entier !!" );
			 alert.setContentText("Veuillez r�essayer.");
			 alert.showAndWait();
			 creer=false;
		 }
		 if(creer && !modif)
		 {
			 bool= Hexalearn.deplacementY(Model.objetComplex,depY, pane);
			 if(bool)
				Model.oldDepY=depY;
			 else
				 Model.oldDepY=0;
			 if (!bool&&Model.objetComplex.getTranslateY()!=0)
			 {
				 MainApp.playSound("warning");
			    alert.setTitle("ATTENTION");
		       	alert.setHeaderText("Le deplacement par rapport � l'axe des ordonn�es de "+depy.getText()+" de l'objet n'est pas possible \nsinon votre objet sortira de la grille!!" );
		       	alert.setContentText("Veuillez r�essayer.");
		       	alert.showAndWait();
			 }
			 else if(Model.objetComplex.getTranslateY()==0&&Model.oldDepY==0)
			 {
				 MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
		       	alert.setHeaderText("Veuillez selectionner l'objet avant \nde le deplacer sur l'axe des ordonn�es  !!" );
		       	alert.setContentText("Veuillez r�essayer.");
		       	alert.showAndWait();
			 }
		 }
	 }

	 /**la fonction qui traite la rotation **/
	 @FXML
	 public void rotation() {
	 	Alert alert = new Alert(AlertType.WARNING);
		boolean bool,creer=true;
	    angle.setEditable(true);
	    double angle1=0;
	    try
	    {
	    	angle1=Double.parseDouble(angle.getText());
	    }
	    catch(NumberFormatException e)
	    {
	    	MainApp.playSound("warning");
	    	alert.setTitle("ATTENTION");
	    	alert.setHeaderText("L'angle de rotation doit �tre un flotant ou un entier !!" );
	    	alert.setContentText("Veuillez r�essayer.");
	    	alert.showAndWait();
	    	creer=false;
		 }
	    if(creer && !modif)
	    {
	    	bool= Hexalearn.rotation(Model.objetComplex,angle1, pane);
	    	if(bool)
	    		Model.oldRotation=angle1;
	    	else
	    		Model.oldRotation=0;
	    	if (!bool&&Model.objetComplex.getRotate()!=0)
	    	{
	    		MainApp.playSound("warning");
	    		alert.setTitle("ATTENTION");
	    		alert.setHeaderText("La rotation de "+angle.getText()+" de votre objet n'est pas possible \nsinon votre objet va sortir de la grille!!" );
	    		alert.setContentText("Veuillez r�essayer.");
	    		alert.showAndWait();
	    	}
	    	else if(Model.objetComplex.getRotate()==0&&Model.oldRotation==0)
	    	{
	    		MainApp.playSound("warning");
	    		alert.setTitle("ATTENTION");
	    		alert.setHeaderText("Veuillez selectionner l'objet avant \nde lui appliquer une rotation !!" );
	    		alert.setContentText("Veuillez r�essayer.");
	    		alert.showAndWait();
	    	}
	   	}
	 }

	 /** la fonction qui traite la redimension d'un objet par une echelle **/
	 @FXML
	 public void Redimension() {
		 Alert alert = new Alert(AlertType.WARNING);
		 boolean bool,creer=true;
		 echelle.setEditable(true);
		 double echelle1=0;
		 try
		 {
			 echelle1=Double.parseDouble(echelle.getText());
		 }
		 catch (NumberFormatException e)
		 {
			 MainApp.playSound("warning");
			 alert.setTitle("ATTENTION");
		     alert.setHeaderText("L'�chelle doit �tre un flotant ou un entier!!" );
		     alert.setContentText("Veuillez r�essayer.");
		     alert.showAndWait();
		     creer=false;
		 }
		 if(creer && !modif)
		    {
		    	bool= Hexalearn.redimension(Model.objetComplex,echelle1, pane);
		    	if(bool)
		    		Model.oldRedimension=echelle1;
		    	else
		    		Model.oldRedimension=0;

		    	if (!bool&&Model.objetComplex.getScaleX()!=1)
		    	{
		    		MainApp.playSound("warning");
		    		alert.setTitle("ATTENTION");
		    		alert.setHeaderText("Le redimension de "+echelle.getText()+" de votre objet n'est pas possible \nsinon il sortira de la grille !!" );
		    		alert.setContentText("Veuillez r�essayer.");
		    		alert.showAndWait();
		    	}
		    	else if(Model.objetComplex.getScaleX()==1&&Model.oldRedimension==0)
		    	{
		    		MainApp.playSound("warning");
		    		alert.setTitle("ATTENTION");
		    		alert.setHeaderText("Veuillez selectionner l'objet avant \nde lui appliquer une redimension !!" );
		    		alert.setContentText("Veuillez r�essayer.");
		    		alert.showAndWait();
		    	}
		   	}
	 }


/**enregistrer une animation **/

	 @FXML
	 public double[] enregClicked(){


		 MainApp.playSound("ClickBouton");

		 Timeline timeline1=new Timeline();
		 Path path=new Path();
		 double deplacementx,deplacementy;
		 Alert alert = new Alert(AlertType.WARNING);
		 try{
			 if (!enreg.isSelected())
			 {
				 enreg.setStyle("-fx-color: grey;");
				 if (! Hexalearn.deplacementX(Double.parseDouble(depx.getText())*12.1, pane) || ! Hexalearn.deplacementX(Double.parseDouble(depy.getText())*-6.4, pane) || ! Hexalearn.redimension(Double.parseDouble(echelle.getText()), pane)|| ! Hexalearn.rotation(Double.parseDouble(angle.getText()), pane))
				 {
					 MainApp.playSound("warning");
					 alert.setTitle("ATTENTION");
					 alert.setHeaderText("Impossible de lancer l'animation\n- V�rifier que vous avez bien selectionner l'objet .\n- Et que les proportions de l'animation ne sont pas trop grandes. !!" );
					 alert.setContentText("Veuillez r�essayer.");
					 alert.showAndWait();
				 }
				 else if(axeTemps.getValue()==0)
				 {
					 alert.setTitle("ATTENTION");
					 alert.setHeaderText("Veuillez selectionner votre temps !!" );
					 alert.setContentText("Veuillez r�essayer.");
					 alert.showAndWait();
				 }
				 else if ( Hexalearn.deplacementX(Double.parseDouble(depx.getText())*12.1, pane) &&  Hexalearn.deplacementX(Double.parseDouble(depy.getText())*-6.4, pane) && Hexalearn.redimension(Double.parseDouble(echelle.getText()), pane) &&  Hexalearn.rotation(Double.parseDouble(angle.getText()), pane)){

				 if(!pa.equals(Model.objetComplex)) {
					 cpt++;
					 pa.setDisable(true);
				 }
				 deplacementx=Model.objetComplex.getTranslateX();
				 deplacementy=Model.objetComplex.getTranslateY();
				 path=Model.objetComplex;

				 if (n==0)
				 {
						final KeyValue initX = new KeyValue(Model.objetComplex.layoutXProperty(),0);
						final KeyValue initY = new KeyValue(Model.objetComplex.layoutYProperty(),0);
						final KeyValue initScaleX = new KeyValue(Model.objetComplex.scaleXProperty(),1);
						final KeyValue initScaleY = new KeyValue(Model.objetComplex.scaleYProperty(),1);
						final KeyValue initRotate = new KeyValue(Model.objetComplex.rotateProperty(),0);
						final KeyValue initColor = new KeyValue(Model.objetComplex.fillProperty(),clr);
						final KeyFrame kf = new KeyFrame(Duration.seconds(0),initX,initY,initScaleX,initScaleY,initRotate,initColor);
						timeline.getKeyFrames().add(kf);
						n++;
				   }
				   double ech,x,y,rot;
				   ech=Model.objetComplex.scaleXProperty().get();
				   x=Model.objetComplex.layoutXProperty().get();
				   y=Model.objetComplex.layoutYProperty().get();
				   rot=Model.objetComplex.rotateProperty().get();

				   final KeyValue initX = new KeyValue(Model.objetComplex.layoutXProperty(),x);
				   final KeyValue initY = new KeyValue(Model.objetComplex.layoutYProperty(),y);
				   final KeyValue initScaleX = new KeyValue(Model.objetComplex.scaleXProperty(),ech);
				   final KeyValue initScaleY = new KeyValue(Model.objetComplex.scaleYProperty(),ech);
				   final KeyValue initRotate = new KeyValue(Model.objetComplex.rotateProperty(),rot);
				   final KeyValue initColor=new KeyValue(Model.objetComplex.fillProperty(),clr);
				   final KeyFrame kf2 = new KeyFrame(Duration.seconds(0),initX,initY,initScaleX,initScaleY,initRotate,initColor);
				   timeline1.getKeyFrames().add(kf2);

				   final KeyValue keyValueX = new KeyValue(Model.objetComplex.layoutXProperty(),x+Double.parseDouble(depx.getText())*12.1);
				   final KeyValue keyValueY = new KeyValue(Model.objetComplex.layoutYProperty(),y+Double.parseDouble(depy.getText())*-6.4);
				   final KeyValue keyValueScaleX = new KeyValue(Model.objetComplex.scaleXProperty(),ech*Double.parseDouble(echelle.getText()));
				   final KeyValue keyValueScaleY = new KeyValue(Model.objetComplex.scaleYProperty(),ech*Double.parseDouble(echelle.getText()));
				   final KeyValue keyValueRotate = new KeyValue(Model.objetComplex.rotateProperty(),rot+Double.parseDouble(angle.getText()));
				   final KeyValue keyValueColor = new KeyValue(Model.objetComplex.fillProperty(),couleur.getValue());

				   tab[0]=x;
				   tab[1]=y;
				   tab[2]=ech;
				   tab[3]=rot;
				   tab[4]=Double.parseDouble(depx.getText())*12.1+x;
				   tab[5]=Double.parseDouble(depy.getText())*-6.4+y;
				   tab[6]=Double.parseDouble(echelle.getText())*ech;
				   tab[7]=Double.parseDouble(angle.getText())+rot;
				   tab[8]=temps;
				   tab[9]=axeTemps.valueProperty().doubleValue();
				   tab[10]=clr.getBlue();
				   tab[11]=clr.getRed();
				   tab[12]=clr.getGreen();
				   tab[13]=couleur.getValue().getBlue();
				   tab[14]=couleur.getValue().getRed();
				   tab[15]=couleur.getValue().getGreen();

				   duree=axeTemps.valueProperty().doubleValue()-duree;
				   if(duree<=0)
				   {
					   MainApp.playSound("warning");
					   alert.setTitle("ATTENTION");
					   alert.setHeaderText("Veuillez selectionner un autre temps superieur au pr�c�dant !!" );
					   alert.setContentText("Veuillez r�essayer.");
					   alert.showAndWait();
				   }

				   else
				   {
					   pa=Model.objetComplex;
					   final KeyFrame kf = new KeyFrame(Duration.seconds(axeTemps.valueProperty().doubleValue()),keyValueX,keyValueY,keyValueScaleX,keyValueScaleY,keyValueRotate,keyValueColor);
					   final KeyFrame kf1 = new KeyFrame(Duration.seconds(duree),keyValueX,keyValueY,keyValueScaleX,keyValueScaleY,keyValueRotate,keyValueColor);
					   timeline.getKeyFrames().add(kf);
					   timeline1.getKeyFrames().add(kf1);
					   timeline1.play();
					   Hexalearn.sauvegarder(path,tab,file,cpt,deplacementx,deplacementy);
					   temps=tab[9];
					   tab[9]=0;
				   }
				 }
				 modif=false;
			   }
			   else {
				   enreg.setStyle("-fx-color: red;");
				   axeTemps.setDisable(false);
				   field.setDisable(false);
				   play.setDisable(false);
				   depx.setText("0");
				   depy.setText("0");;
				   echelle.setText("1");
				   angle.setText("0");;
				   modif=true;
				   bool=true;
				   clr=(Color) Model.objetComplex.getFill();
				   couleur.setValue((Color) Model.objetComplex.getFill());
				   if(n==0) {
					   file=new File("src\\Comptes\\test.hexa");
					   file.deleteOnExit();
				   }
			   }
		   }catch (final java.lang.NullPointerException k){
		   }
		   return tab;
	   }


	/** voir une animation deja enregistrer **/
	 @FXML
	 public void playAnimation(){
		 MainApp.playSound("ClickBouton");
		 play.setDisable(false);
		 stop.setDisable(false);
		 pause.setDisable(false);
		 field.setVisible(false);
		 axeTemps.setVisible(false);
		 enreg.setDisable(true);
		 couleur.setDisable(true);
		 timeline.play();
		 currentRateText.setVisible(true);
		 currentRateText.setBoundsType(TextBoundsType.VISUAL);
		 barre.setVisible(true);
		 timeline.currentTimeProperty().addListener((Observable ov) -> {
			 barre.setStyle("-fx-color: green;");
			 double time = (double) timeline.getCurrentTime().toSeconds();
			 barre.setProgress(time/timeline.totalDurationProperty().getValue().toSeconds());
			 currentRateText.setText((int) time + "s");
	       });
		 timeline.setOnFinished(new EventHandler<ActionEvent> (){
			 public void handle(ActionEvent event) {
	 				play.setSelected(false);
	 				play.setDisable(false);
	 		 		field.setVisible(true);
	 		 		axeTemps.setVisible(true);
	 		 		enreg.setDisable(false);
	 		 		couleur.setDisable(false);
	 		 		barre.setVisible(false);
	 		 		currentRateText.setVisible(false);
	 			}
	 		});
	 }


		/** stoper l'animation **/
	 @FXML
	 public void stopAnimation(){
		 MainApp.playSound("ClickBouton");
		 timeline.stop();
		 if (bool) play.setSelected(false);
		 if (bool) play.setDisable(false);
		 field.setVisible(true);
		 axeTemps.setVisible(true);
		 enreg.setDisable(false);
		 couleur.setDisable(false);
		 barre.setVisible(false);
		 currentRateText.setVisible(false);
		 Polygone.getTransition().stop();
	 }

	 /**faire une pause a l'animation **/
	 @FXML
	 public void pauseAnimation(){
		 MainApp.playSound("ClickBouton");
		 timeline.pause();
		 Polygone.getTransition().pause();
		 if (bool) play.setDisable(false);
	 }

	 public void initialize(URL arg0, ResourceBundle arg1) {
		field.textProperty().bindBidirectional(axeTemps.valueProperty(),NumberFormat.getIntegerInstance());
		depx.setText("0");
		depy.setText("0");
		echelle.setText("1");
		angle.setText("0");
		couleur.setValue(Color.BLUE);
	}

	 /**changer la couleur du polygone ou de l'objet complex **/
 	public void couleur()
 	{
 		Model.objetComplex.setFill(couleur.getValue());
 	}

 	/** revenir a fenetre de l'accuil **/
	@FXML
	public void ActionBtnReturnHome(){
		MainApp.playSound("ClickBouton");
		MainApp.WorkSpaceStage.close();
		MainApp.AcceuilStage.show();
	}

	/** selectionner un fichier pour l'enregistrement **/
	@FXML
	public void selectionerFile(){

		MainApp.playSound("ClickBouton");
		Alert alert = new Alert(AlertType.WARNING);
		if(MainApp.element.getNom().length()==0){
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
			alert.setHeaderText("Veuillez connecter  !!" );
			alert.setContentText("Veuillez r�essayer.");
			alert.showAndWait();
		}
		else{
		fc.setInitialDirectory(new File("comptes/"+MainApp.element.getNom()));
		fc.getExtensionFilters().addAll(new ExtensionFilter("Animation (."+MainApp.element.getNom()+") " , "*."+MainApp.element.getNom()));
		fc.setTitle("Ouvrir une Animation");
		selectedFile= fc.showOpenDialog(null);
		if (selectedFile!=null) {
			reset();
			ObjectInputStream ois = null;
			Object enreg;
			FileInputStream fichier;
			try {
				fichier = new FileInputStream(selectedFile);
				ois = new ObjectInputStream(fichier);
				enreg = (Object)ois.readObject();
				if (enreg instanceof SauvComplexe) 	lireEnreg();
				if (enreg instanceof SauvPath) 	lireTrajectoire();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}finally {
				if (ois != null)
					try {
						ois.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/** lire un enregistrement **/
	public  void lireEnreg(){
		 Hexalearn.lire(selectedFile,timeline2,pane);
    }

	/** lire un enregistrement d'une animation sur une trajectoire **/
	public  void lireTrajectoire(){
		 Hexalearn.lireTrajectoire(selectedFile,pane);
    }

	/**sauvegarder une animation **/
	@FXML
	public void sauvgarder(){
		MainApp.playSound("ClickBouton");
		Alert alert = new Alert(AlertType.WARNING);
		if(MainApp.element.getNom().length()==0){
			MainApp.playSound("warning");
			alert.setTitle("ATTENTION");
			alert.setHeaderText("Veuillez connecter  !!" );
			alert.setContentText("Veuillez r�essayer.");
			alert.showAndWait();
		}
		else{
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Save file");
			fileChooser.setInitialFileName("Animation."+MainApp.element.getNom());
			fileChooser.setInitialDirectory(new File("comptes/"+MainApp.element.getNom()));
			File savedFile = fileChooser.showSaveDialog(null);
			if (savedFile!=null)
			{
				if(file!=null)
					file.renameTo(savedFile);
				else{
					MainApp.playSound("warning");
					alert.setTitle("ATTENTION");
					alert.setHeaderText("Veuillez cr�er une animation  !!" );
					alert.setContentText("Veuillez r�essayer.");
					alert.showAndWait();
				}
			}
		}
	}

	/**supprimer un polygone ou un objet complexe **/
	@FXML
	public void supp(){
		MainApp.playSound("ClickBouton");
		bool=false;
		MainApp.groupe.getChildren().remove(Model.objetComplex);
		MainApp.groupe.getChildren().remove(Model.path);
		Model.getListPoly().remove(Model.objetComplex);
	}

	/**nettoyer la grille **/

	@FXML
	public void reset(){
		if(file!=null) file.delete();
		MainApp.playSound("ClickBouton");
		while(!Model.getListPoly().isEmpty())
			for (int i=0;i<Model.getListPoly().size();i++)
			{
				if (Model.getListPoly().get(i) instanceof Path)
					Model.getList().add(Model.getListPoly().get(i));
				MainApp.groupe.getChildren().remove(Model.getListPoly().get(i));
				Model.getListPoly().remove(i);
			}
	}


	/**nettoyer la grille dans le cas ou on change d'utilisateur **/
	public static void resett(){



		MainApp.playSound("ClickBouton");


			for (int i=0;i<Model.getListPoly().size();i++)
			{


				MainApp.groupe.getChildren().remove(Model.getListPoly().get(i));


			}
    Model.getList().clear();
    Model.getListPoly().clear();


	}

	/**regrouper 2 polygones pour construire un objet complexe **/
	@FXML
	public void regrouper(){
		MainApp.playSound("ClickBouton");
		Alert alert = new Alert(AlertType.WARNING);
		if(regrouper.isSelected()) {
		}
		else {
			Shape shape=Shape.intersect(Model.objetComplex,Model.prev);
			if (shape.getBoundsInLocal().getWidth()!=-1)
			{
				Model.prev.setOpacity(0);
				Model.objetComplex.setOpacity(0);
				Path path=(Path) Path.union(Model.prev,Model.objetComplex);
				Model.prev=Model.objetComplex;
				Model.objetComplex=path;
			    Hexalearn.depSouris(path);
			    path.setOnMouseClicked(e->Model.select(path));
			    path.setFill(Color.color(Math.random(),Math.random(),Math.random(), 1));
				DecimalFormat df = new DecimalFormat ( ) ;
				df.setMaximumFractionDigits ( 1 ) ; //arrondi � 2 chiffres apres la virgules
				df.setMinimumFractionDigits ( 1 ) ;
				df.setDecimalSeparatorAlwaysShown ( true ) ;
				path.setOnMouseEntered(e -> Tooltip.install(   path, new Tooltip("Pivot =(x = "+ (double)  ((int)((((path.getBoundsInParent().getMaxX()+path.getBoundsInParent().getMinX())/2-632.25)/10.08331298828125)*100))/100+", y = "+(double)((int)(((path.getBoundsInParent().getMaxY()+path.getBoundsInParent().getMinY())/2-355.5)/-9.69696044921875*100))/100+")")));

				MainApp.groupe.getChildren().add(path);
				Model.getListPoly().add(path);
			}
			else{
				MainApp.playSound("warning");
				alert.setTitle("ATTENTION");
				alert.setHeaderText("No intersection !" );
				alert.setContentText("Veuillez r�essayer.");
				alert.showAndWait();
			}
		}

	}

	/** dessiner une trajectoire **/
	@FXML
	public void DrawPath(){
		MainApp.playSound("ClickBouton");
		Trajectoire t=new Trajectoire();
		t.trajectoire();
		axeTemps.setDisable(false);
		pause.setDisable(false);
		stop.setDisable(false);
	  }
/** Animer un polygone ou un objet complexe sur une trajectoire **/
	public void ActionBtnAnimateOnPath(){

		if (Polygone.AnimateOnPath(axeTemps.getValue()))
		{	MainApp.playSound("ClickBouton");
			String[] tab=Polygone.getTrajPoly();
			if (file!=null) file.delete();
			file=new File("src\\Comptes\\test.hexa");
			 Hexalearn.sauvTraj(file,tab[0],tab[1],axeTemps.getValue());
			play.setDisable(true);
		}
	}

/** ajouter une nouvelle animation **/
	public void nouvelleAnimation(){
		if (file!=null) file.delete();
		 reset();
	 }

	public void chooseFile() throws IOException{
		String imagepath="";
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open File");
		File file = chooser.showOpenDialog(new Stage());
		if(file != null) {
			try {
				imagepath = file.toURI().toURL().toString();
				Image image = new Image(imagepath);
				profilePic.setImage(image);
				File f2=new File("bin/profiles/"+MainApp.element.getNom()+".PNG");
				Model.copyFile(file, f2);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}

	@FXML
	private void ActionBtnCreation() {
		MainApp.playSound("ClickBouton");
		tabPaneW.getSelectionModel().select(0);
	}

	@FXML
	private void ActionBtnModification() {
		MainApp.playSound("ClickBouton");
		tabPaneW.getSelectionModel().select(1);
	}

	@FXML
	private void ActionBtnAnimation() {
		MainApp.playSound("ClickBouton");
		tabPaneW.getSelectionModel().select(2);
	}

	/**fermer l'application **/
	@FXML
	public void close(){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		MainApp.playSound("warning");
		alert.setTitle("Confirmation");
		alert.setHeaderText("Voulez-vous vraiment quitter !" );
		alert.getButtonTypes().set(0, new ButtonType("Oui"));
		alert.getButtonTypes().set(1, new ButtonType("Non"));
		alert.showAndWait();
		if (alert.getResult().getText().equals("Oui")) {
			MainApp.WorkSpaceStage.close();
			MainApp.AcceuilStage.close();
		}
	}

	/** Afficher la fenetre apropos **/
	@FXML
	public void apropos(){
		Alert alert = new Alert(AlertType.INFORMATION);
		MainApp.playSound("warning");
		alert.setTitle("A propos");
		alert.setHeaderText("HexaLearn 1.0 ");
		alert.setContentText("HexaLearn est une application distin�e aux coll�giens;\nelle permet de g�n�rer les mouvements de bases contenant\nconsernant es polygones mais aussi de cr�er des animation gr�ce � ces derniers");
		alert.showAndWait();
	}

	/** afficher la page html de l'aide **/
	@FXML
    public void aide(){
    	try {
            Desktop.getDesktop().browse(new URI("page.htm"));
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e1) {
            e1.printStackTrace();
        }
        }

/** couper  le son de l'application **/
	@FXML
	public void couperSon()
	{
		Image image;
		if (MainApp.playsound==true){
			MainApp.playsound=false;
			image=new Image("images/son2.jpg");
			son.setImage(image);
		}
		else {
			image=new Image("images/son.jpg");
			son.setImage(image);
			MainApp.playsound=true;
		}
	}

}