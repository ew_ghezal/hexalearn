package application;

/**La classe principale de l'application **/


import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import ComPack.Model.Element;
import ComPack.Model.Eleve;

public class MainApp extends Application {

	public static Group groupe = new Group();
	public static Group animation = new Group();
	public static Stage AcceuilStage;
	public static Stage WorkSpaceStage;
	public static Stage aideEnLigne1;
	public static Stage aideEnLigne2;
	public static Stage aideEnLigne3;
	public static Stage aideEnLigne4;
	public static Stage aideEnLigne5;
	public static Stage aideEnLigne6;
	public static Scene sceneWS;
	public static Element element = new Eleve();
	public static boolean playsound = true;

	/**lire un fichier son **/
	public static void playSound(String nom) {
		try {
			AudioInputStream audioInputStream = AudioSystem
					.getAudioInputStream(new File("src/sound/" + nom + ".wav")
							.getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			if (playsound)
				clip.start();
		} catch (Exception ex) {
			System.out.println("Error with playing sound.");
			ex.printStackTrace();
		}
	}

	/**Affichage de l'interface principale**/
	@Override
	public void start(Stage primaryStage) {
		try {

			AcceuilStage = primaryStage;
			AcceuilStage.setTitle("HexaLearn");
			AcceuilStage.getIcons().add(new Image("file:src/images/icone.png"));

			Parent root = FXMLLoader.load(getClass().getResource(
					"../ComPack/View/Acceuil.fxml"));
			Scene scene = new Scene(root);
			AcceuilStage.setScene(scene);
			AcceuilStage.setResizable(false);

			AcceuilStage.show();

			WorkSpaceStage = new Stage();
			WorkSpaceStage.setResizable(true);
			Parent rootWS = FXMLLoader.load(getClass().getResource(
					"../ComPack/View/WorkSpace.fxml"));
			sceneWS = new Scene(rootWS);
			groupe.getChildren().add(rootWS);
			sceneWS.setRoot(groupe);
			WorkSpaceStage.setScene(sceneWS);
			WorkSpaceStage.getIcons().add(
					new Image("file:src/images/icone.png"));

			WorkSpaceStage.centerOnScreen();
			WorkSpaceStage.setResizable(false);
			/*************************************************/

		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	/**Affichage des différents tutoriels pour l'aide en ligne ***/

	/**Tutorie 1**/
	public static boolean ShowAideEnLigne1() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/01.PNG").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/02.PNG").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/03.PNG").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/04.PNG").toString());
		final Image image5 = new Image(MainApp.class.getResource(
				"../images/05.PNG").toString());
		final Image image6 = new Image(MainApp.class.getResource(
				"../images/06.PNG").toString());
		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);
		final ImageView imag5 = new ImageView(image5);
		final ImageView imag6 = new ImageView(image6);
		animation = new Group(imag1);
		Timeline tl = new Timeline();
		tl.setCycleCount(1);
		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});
		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});
		KeyFrame key5 = new KeyFrame(Duration.seconds(20),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag6);
					}
				});
		KeyFrame key6 = new KeyFrame(Duration.seconds(24),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag6);
					}
				});
		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4, key5, key6);
		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne1.close();
			}
		});
		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne1 = new Stage();
		aideEnLigne1.setScene(scene);
		aideEnLigne1.show();
		return true;
	}

	/**Tutorie 2**/
	public static boolean ShowAideEnLigne2() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/07.PNG").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/08.PNG").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/09.PNG").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/10.PNG").toString());
		final Image image5 = new Image(MainApp.class.getResource(
				"../images/11.PNG").toString());

		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);
		final ImageView imag5 = new ImageView(image5);

		animation = new Group(imag1);

		Timeline tl = new Timeline();

		tl.setCycleCount(1);

		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {

					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});
		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});

		KeyFrame key5 = new KeyFrame(Duration.seconds(20),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});
		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4, key5);

		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne2.close();
			}
		});

		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne2 = new Stage();
		aideEnLigne2.setScene(scene);

		aideEnLigne2.show();
		return true;
	}

	/**Tutorie 3**/
	public static boolean ShowAideEnLigne3() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/12.PNG").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/13.PNG").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/14.PNG").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/15.PNG").toString());
		final Image image5 = new Image(MainApp.class.getResource(
				"../images/16.PNG").toString());
		final Image image6 = new Image(MainApp.class.getResource(
				"../images/17.PNG").toString());
		final Image image7 = new Image(MainApp.class.getResource(
				"../images/18.PNG").toString());
		final Image image8 = new Image(MainApp.class.getResource(
				"../images/19.PNG").toString());

		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);
		final ImageView imag5 = new ImageView(image5);
		final ImageView imag6 = new ImageView(image6);
		final ImageView imag7 = new ImageView(image7);
		final ImageView imag8 = new ImageView(image8);

		animation = new Group(imag1);

		Timeline tl = new Timeline();

		tl.setCycleCount(1);

		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {

					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});
		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});
		KeyFrame key5 = new KeyFrame(Duration.seconds(20),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag6);
					}
				});

		KeyFrame key6 = new KeyFrame(Duration.seconds(24),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag7);
					}
				});
		KeyFrame key7 = new KeyFrame(Duration.seconds(28),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag8);
					}
				});
		KeyFrame key8 = new KeyFrame(Duration.seconds(32),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag8);
					}
				});

		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4, key5, key6,
				key7, key8);

		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne3.close();
			}
		});

		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne3 = new Stage();
		aideEnLigne3.setScene(scene);

		aideEnLigne3.show();

		return true;
	}


	/**Tutorie 4**/
	public static boolean ShowAideEnLigne4() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/20.png").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/21.png").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/22.png").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/23.png").toString());
		final Image image5 = new Image(MainApp.class.getResource(
				"../images/24.png").toString());
		final Image image6 = new Image(MainApp.class.getResource(
				"../images/25.png").toString());
		final Image image7 = new Image(MainApp.class.getResource(
				"../images/26.png").toString());
		final Image image8 = new Image(MainApp.class.getResource(
				"../images/27.png").toString());

		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);
		final ImageView imag5 = new ImageView(image5);
		final ImageView imag6 = new ImageView(image6);
		final ImageView imag7 = new ImageView(image7);
		final ImageView imag8 = new ImageView(image8);

		animation = new Group(imag1);

		Timeline tl = new Timeline();

		tl.setCycleCount(1);

		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {

					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});
		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});
		KeyFrame key5 = new KeyFrame(Duration.seconds(20),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag6);
					}
				});

		KeyFrame key6 = new KeyFrame(Duration.seconds(24),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag7);
					}
				});
		KeyFrame key7 = new KeyFrame(Duration.seconds(28),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag8);
					}
				});
		KeyFrame key8 = new KeyFrame(Duration.seconds(32),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag8);
					}
				});

		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4, key5, key6,
				key7, key8);

		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne4.close();
			}
		});

		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne4 = new Stage();
		aideEnLigne4.setScene(scene);

		aideEnLigne4.show();

		return true;
	}

	/**Tutorie 5**/
	public static boolean ShowAideEnLigne5() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/28.PNG").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/29.PNG").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/30.PNG").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/31.PNG").toString());
		final Image image5 = new Image(MainApp.class.getResource(
				"../images/32.PNG").toString());
		final Image image6 = new Image(MainApp.class.getResource(
				"../images/33.PNG").toString());
		final Image image7 = new Image(MainApp.class.getResource(
				"../images/34.PNG").toString());
		final Image image8 = new Image(MainApp.class.getResource(
				"../images/35.PNG").toString());
		final Image image9 = new Image(MainApp.class.getResource(
				"../images/36.PNG").toString());
		final Image image10 = new Image(MainApp.class.getResource(
				"../images/37.PNG").toString());

		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);
		final ImageView imag5 = new ImageView(image5);
		final ImageView imag6 = new ImageView(image6);
		final ImageView imag7 = new ImageView(image7);
		final ImageView imag8 = new ImageView(image8);
		final ImageView imag9 = new ImageView(image9);
		final ImageView imag10 = new ImageView(image10);

		animation = new Group(imag1);

		Timeline tl = new Timeline();

		tl.setCycleCount(1);

		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {

					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});
		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag5);
					}
				});
		KeyFrame key5 = new KeyFrame(Duration.seconds(20),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag6);
					}
				});

		KeyFrame key6 = new KeyFrame(Duration.seconds(21),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag7);
					}
				});
		KeyFrame key7 = new KeyFrame(Duration.seconds(22),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag8);
					}
				});
		KeyFrame key8 = new KeyFrame(Duration.seconds(23),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag9);
					}
				});
		KeyFrame key9 = new KeyFrame(Duration.seconds(24),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag10);
					}
				});
		KeyFrame key10 = new KeyFrame(Duration.seconds(28),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag10);
					}
				});

		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4, key5, key6,
				key7, key8, key9, key10);

		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne5.close();
			}
		});

		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne5 = new Stage();
		aideEnLigne5.setScene(scene);

		aideEnLigne5.show();

		return true;
	}

	/**Tutorie 6**/
	public static boolean ShowAideEnLigne6() {


		final Image image1 = new Image(MainApp.class.getResource(
				"../images/38.PNG").toString());
		final Image image2 = new Image(MainApp.class.getResource(
				"../images/39.PNG").toString());
		final Image image3 = new Image(MainApp.class.getResource(
				"../images/40.PNG").toString());
		final Image image4 = new Image(MainApp.class.getResource(
				"../images/41.PNG").toString());

		final ImageView imag1 = new ImageView(image1);
		final ImageView imag2 = new ImageView(image2);
		final ImageView imag3 = new ImageView(image3);
		final ImageView imag4 = new ImageView(image4);

		animation = new Group(imag1);

		Timeline tl = new Timeline();

		tl.setCycleCount(1);

		KeyFrame key1 = new KeyFrame(Duration.seconds(4),
				new EventHandler<ActionEvent>() {

					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag2);
					}
				});
		KeyFrame key2 = new KeyFrame(Duration.seconds(8),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag3);
					}
				});
		KeyFrame key3 = new KeyFrame(Duration.seconds(12),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});

		KeyFrame key4 = new KeyFrame(Duration.seconds(16),
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent t) {
						animation.getChildren().setAll(imag4);
					}
				});

		tl.getKeyFrames().addAll(key1, key2, key2, key3, key4);

		tl.play();
		tl.setOnFinished(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				aideEnLigne6.close();
			}
		});

		final Group root = new Group(animation);
		Scene scene = new Scene(root, 1332, 700);
		aideEnLigne6 = new Stage();
		aideEnLigne6.setScene(scene);

		aideEnLigne6.show();

		return true;
	}

	/******Affichage de la grille**********/
	public static boolean ShowWorkSpaceWindow() {
		/*************************************/

		WorkSpaceStage
				.getIcons()
				.add(new Image(
						"file:images/icone.png"));
		WorkSpaceStage.setTitle("Espace de Travail");
		AcceuilStage.hide();
		WorkSpaceStage.show();

		return true;
	}

	public static PasswordField pf = new PasswordField();
	public static TextField txtUserName = new TextField();
	public static Button btnLogin = new Button("Entrer");

	/**Afichage de la fenetre login **/
	public static void ShowLoginWindow() {

		Stage primaryStage = new Stage();
		primaryStage.setTitle("Fenetre de Login");

		BorderPane bp = new BorderPane();
		bp.setPadding(new Insets(10, 50, 50, 50));

		HBox hb = new HBox();
		hb.setPadding(new Insets(20, 20, 20, 30));

		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(20, 20, 20, 20));
		gridPane.setHgap(5);
		gridPane.setVgap(4);


		Label lblUserName = new Label("Nom d'utilisateur :");
		Label lblPassword = new Label("Mot de passe :");
		final Label lblMessage = new Label();

		gridPane.add(lblUserName, 0, 0);
		gridPane.add(txtUserName, 1, 0);
		gridPane.add(lblPassword, 0, 1);
		gridPane.add(pf, 1, 1);
		gridPane.add(btnLogin, 2, 1);
		gridPane.add(lblMessage, 1, 2);


		Reflection r = new Reflection();
		r.setFraction(0.7f);
		gridPane.setEffect(r);

		DropShadow dropShadow = new DropShadow();
		dropShadow.setOffsetX(5);
		dropShadow.setOffsetY(5);


		Text text = new Text("HEXAGONE");
		text.setFont(Font.font("Courier New", FontWeight.BOLD, 60));
		text.setEffect(dropShadow);
		// Adding text to HBox
		hb.getChildren().add(text);

		bp.setId("bp");
		gridPane.setId("root");
		btnLogin.setId("btnLogin");
		text.setId("text");

		btnLogin.setOnAction(e -> Eleve.seConnecter(txtUserName.getText(),
				pf.getText()));

		bp.setTop(hb);
		bp.setCenter(gridPane);
		Scene scene = new Scene(bp);
		scene.getStylesheets().add("login.css");
		primaryStage.setScene(scene);
		primaryStage.show();
	}


	public Stage getPrimaryStage() {
		return AcceuilStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**Affichage de la fenetre de dialog pour ajouter un compte**/
	public static boolean ShowAjouterCompte() {
		try {
			/*************************************/
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class
					.getResource("../ComPack/View/AjouterCompte.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage loginStage = new Stage();
			loginStage.setTitle("HEXAGONE|Connection");
			loginStage
					.getIcons()
					.add(new Image(
							"file:HEXAGONE.Xa/src/Polygone/View/images/creating_abstract_shape_creation_create-128.png"));
			loginStage.initModality(Modality.WINDOW_MODAL);
			loginStage.initOwner(AcceuilStage);
			Scene scene = new Scene(page);
			loginStage.setScene(scene);
			loginStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
}